#!/bin/bash
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_Base_ini.txt N1415 ForwardDiff am 2000 Base &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_Base.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_EnhBio_ini.txt N1415 ForwardDiff am 2000 EnhBio &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_EnhBio.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_NarrowEps_ini.txt N1415 ForwardDiff am 2000 NarrowEps &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_NarrowEps.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_NitOff_ini.txt N1415 ForwardDiff am 2000 NitOff &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_NitOff.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_DNRAOff_ini.txt N1415 ForwardDiff am 2000 DNRAOff &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_DNRAOff.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_FixedEps_ini.txt N1415 ForwardDiff am 2000 FixedEps &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_FixedEps.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_AnamOff_ini.txt N1415 ForwardDiff am 2000 AnamOff &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_AnamOff.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_Widerf_ini.txt N1415 ForwardDiff am 2000 Widerf &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_Widerf.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_EnhSep_ini.txt N1415 ForwardDiff am 2000 EnhSep &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_EnhSep.log &
julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters_OneStep_ini.txt N1415 ForwardDiff am 2000 OneStep &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000_OneStep.log &
# julia --threads 6 Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters.txt N1415 PolyesterForwardDiff AdvancedHMC 100 &> ../Output/Lucerne_Apr2021_N1415_NLNewton_PolyesterForwardDiff_AdvancedHMC10_100.log &
# julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters.txt N1415 ForwardDiff am 2000 &> ../Output/Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_2000.log &
# julia Nsediment_run.jl Skagerrak_S9 Skagerrak_S9_parameters.txt N1415 ForwardDiff am 5000 &> ../Output/Skagerrak_S9_N1415_NLNewton_ForwardDiff_am_5000.log &
# julia Nsediment_run.jl Bering_MC5 Bering_MC5_parameters.txt N1415 ForwardDiff am 2000 &> ../Output/Bering_MC5_N1415_NLNewton_ForwardDiff_am_2000.log &
# julia Nsediment_run.jl Bering_MC19 Bering_MC19_parameters.txt N1415 ForwardDiff am 2000 &> ../Output/Bering_MC19_N1415_NLNewton_ForwardDiff_am_2000.log &
# julia Nsediment_run.jl Bering_MC29 Bering_MC29_parameters.txt N1415 ForwardDiff am 2000 &> ../Output/Bering_MC29_N1415_NLNewton_ForwardDiff_am_2000.log &
