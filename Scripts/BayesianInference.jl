## ==========================================================
##
## File: BayesianInference.jl
##
## Interface function to MCMC samplers for Bayesian Inference
##
## creation:          November 26, 2021 -- Peter Reichert
## last modification: July 10    , 2024 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ==========================================================


# Load packages:
# ==============

import Statistics
import Random
import Dates

import ForwardDiff
import PolyesterForwardDiff
# import Zygote
# import ReverseDiff

import LogDensityProblems
import LogDensityProblemsAD
import TransformVariables
import TransformedLogDensities
import AbstractMCMC
import MCMCChains
import MCMCDiagnosticTools
import AdvancedHMC
import DynamicHMC
import AdaptiveMCMC

include("TransPar.jl")


# MCMC sampler interface:
# =======================

# choices for MCMCalg:
#   "AdvancedHMC"    Hamiltonian Monte Carlo algorithm from  `AdvancedHMC.jl`
#   "DynamicHMC"     Hamiltonian Monte Carlo algorithm from  `DynamicHMC.jl`
#   "am"             adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`
#   "ram"            robust adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`
#   "asm"            adaptive scaling Metropolis algorithm from  `AdaptiveMCMC.jl`
#   "aswam"          adaptive scaling within adaptive Metropolis algorithm from  `AdaptiveMCMC.jl`

# choices for AutoDiff:
#   "ForwardDiff"
#   "PolyesterForwardDiff"
#   "Zygote"
#   "ReverseDiff"

function BayesianInference(logpost::Function, 
                           par_ini::Vector{Float64}, 
                           parnames::Vector{String};
                           Trans          = missing,
                           AutoDiff       = "ForwardDiff",
                           MCMCalg        = "AdvancedHMC",
                           n_samples::Int = 1000, 
                           n_adapt::Int   = n_samples÷5,
                           thin           = 5,             # does not apply to HMC algorithms
                           n_leapfrog     = 0,             # AdvancedHMC: 0=NUTS, >0 fixed number of leapfrog steps
                           initial_step   = missing,       # AdvancedHMC: initial step size
                           chunksize      = 8,             # chunk size for ForwardDiff
                           verbose        = true)

    if ismissing(Trans)
        Trans = Array{TransPar}(undef,length(par_ini))
        for i in 1:length(par_ini)
            Trans[i] = TransParUnbounded()
        end
    end

    par_trans = TransformForward(Trans,par_ini)

    function logpost_trans(par_trans)
        if ! all(isfinite.(par_trans)); return Float64(-Inf); end
        par = TransformBackward(Trans,par_trans)
        return logpost(par) + CorrectLogPdf(Trans,par_trans)
    end

    # produce transformed logposterior compatible with LogDensityProblems interface:
    # ------------------------------------------------------------------------------

    ext1 = ""
    ext2 = ""
    if verbose

        if MCMCalg == "AdvancedHMC"; if n_leapfrog==0; ext1="-NUTS"; else ext1=string("-",n_leapfrog,"LeapfrogSteps"); end; end
        if (MCMCalg == "AdvancedHMC") | (MCMCalg == "DynamicHMC"); ext2=string("-",AutoDiff); end 
        println(string(Dates.now(),": BayesianInference: starting MCMC sampling with algorithm ",MCMCalg,ext1,ext2))
        println(string("                                            ",
                       "number of parameters: ",length(par_ini),", sample size: ",n_samples,
                       if (MCMCalg != "AdvancedHMC") && (MCMCalg != "DynamicHMC"); string(" (thin=",thin,")"); else ""; end,
                       if (MCMCalg == "AdvancedHMC") && (! ismissing(initial_step)); string(", given initial step: ",initial_step); else ""; end))
        flush(stdout)

    end

    if MCMCalg == "AdvancedHMC"

        lpt  = TransformedLogDensities.TransformedLogDensity(TransformVariables.as(Vector,TransformVariables.asℝ,length(par_trans)),logpost_trans)
    
        metric = AdvancedHMC.DiagEuclideanMetric(length(par_trans))
        # metric = AdvancedHMC.DenseEuclideanMetric(length(par_trans))

        function calcgrad_polyesterforwarddiff(x)
            grad = similar(x)
            PolyesterForwardDiff.threaded_gradient!(logpost_trans,grad,x,ForwardDiff.Chunk(chunksize),
                                                    Val{true}())
            return (logpost_trans(x),grad)
        end

        if AutoDiff == "ForwardDiff"
            hamiltonian = AdvancedHMC.Hamiltonian(metric,lpt,ForwardDiff)
        elseif AutoDiff == "PolyesterForwardDiff"
            hamiltonian = AdvancedHMC.Hamiltonian(metric,AdvancedHMC.GaussianKinetic(),logpost_trans,calcgrad_polyesterforwarddiff)
        elseif AutoDiff == "Zygote"
            hamiltonian = AdvancedHMC.Hamiltonian(metric,lpt,Zygote)
        elseif AutoDiff == "ReverseDiff"
            hamiltonian = AdvancedHMC.Hamiltonian(metric,lpt,ReverseDiff)
        else
            error(string("*** BayesianInference:  unknown value of AutoDiff: ",AutoDiff))
        end

        if ismissing(initial_step); initial_step  = AdvancedHMC.find_good_stepsize(hamiltonian,par_trans); end

        if verbose

            println(string(Dates.now(),": BayesianInference: initial step = ",initial_step))
 
        end

        integrator = AdvancedHMC.Leapfrog(initial_step)
        if n_leapfrog > 0
            kernel = AdvancedHMC.HMCKernel(AdvancedHMC.Trajectory{AdvancedHMC.EndPointTS}(integrator,AdvancedHMC.FixedNSteps(n_leapfrog)))
        else
            kernel = AdvancedHMC.HMCKernel(AdvancedHMC.Trajectory{AdvancedHMC.MultinomialTS}(integrator,AdvancedHMC.GeneralisedNoUTurn()))
            # kernel = AdvancedHMC.HMCKernel(AdvancedHMC.Trajectory{AdvancedHMC.SliceTS}(integrator,AdvancedHMC.ClassicNoUTurn()))
        end
        adaptor    = AdvancedHMC.StanHMCAdaptor(AdvancedHMC.MassMatrixAdaptor(metric),
                                                AdvancedHMC.StepSizeAdaptor(0.8, integrator))

        sample_trans, stats_trans = AdvancedHMC.sample(hamiltonian,kernel,par_trans,n_samples, 
                                                       adaptor,n_adapt;progress=true)
        sample = [collect(TransformBackward(Trans,s)) for s in sample_trans]

    elseif MCMCalg == "DynamicHMC"

        if ! ( AutoDiff in ["ForwardDiff","Zygote","ReverseDiff"] )
            error(string("*** BayesianInference: autodiff method ",AutoDiff," not implemented for ",MCMCalg))
        end

        lpt  = TransformedLogDensities.TransformedLogDensity(TransformVariables.as(Vector,TransformVariables.asℝ,length(par_trans)),logpost_trans)
        ∇lpt = LogDensityProblemsAD.ADgradient(Symbol(AutoDiff),lpt)
    
        results = DynamicHMC.mcmc_with_warmup(Random.default_rng(),∇lpt,n_samples;
                                              initialization = (q=par_trans,))

        DynamicHMC.Diagnostics.summarize_tree_statistics(results.tree_statistics)

        sample = transpose(results.posterior_matrix)
        for i in 1:length(par_ini)
            for j in 1:size(sample)[1]
                sample[j,i] = TransformBackward(Trans[i],sample[j,i])
            end
        end

    elseif (MCMCalg == "am") | (MCMCalg == "ram") | (MCMCalg == "asm") | (MCMCalg == "aswam")

        results = AdaptiveMCMC.adaptive_rwm(par_trans,logpost_trans,n_adapt+n_samples;
                                            algorithm = Symbol(MCMCalg),
                                            b         = n_adapt,
                                            thin      = thin)

        sample = transpose(results.X)
        for i in 1:length(par_ini)
            for j in 1:size(sample)[1]
                sample[j,i] = TransformBackward(Trans[i],sample[j,i])
            end
        end

    else

        error(string("*** BayesianInference: unknown value of MCMCalg: ",MCMCalg))

    end

    chains = MCMCChains.Chains(sample,parnames)

    if verbose

        ess = missing
        println(string(Dates.now(),": BayesianInference: MCMC sampling completed (algorithm ",MCMCalg,ext1,ext2,")"))
        try
            ess = MCMCDiagnosticTools.ess(chains)[:,2]
        catch
            ess = missing
        end
        println(string("                                            ",
                       "sample size (real, mean effective): ",size(chains)[1],", ",round(Statistics.mean(ess),digits=1)))
        for i in 1:length(parnames)
            println(string("                                            ",
                           parnames[i],":",
                           " mean = ",round(Statistics.mean(Array(chains)[:,i]),sigdigits=4),
                           ", sd = ",round(Statistics.std(Array(chains)[:,i]),sigdigits=3),
                           if ismissing(ess); ""; else string(", ess = ",round(ess[i],digits=1)); end))
        end
        println("")

        flush(stdout)

    end

    # return chains:
    # --------------

    return chains

end

