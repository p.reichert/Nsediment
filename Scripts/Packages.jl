import Pkg
Pkg.activate(".")
Pkg.instantiate()

println(string("Julia ",VERSION,", number of threads: ",Threads.nthreads()))

Pkg.add("Statistics")
Pkg.add("Random")
Pkg.add("DataFrames")
Pkg.add("CSV")
Pkg.add("DelimitedFiles")
Pkg.add("JLD2")
Pkg.add("Dates")

Pkg.add("Plots")

Pkg.add("ForwardDiff")
Pkg.add("PolyesterForwardDiff")
# Pkg.add("Zygote")
# Pkg.add("ReverseDiff")
Pkg.add("DistributionsAD")
Pkg.add("OrdinaryDiffEq")
Pkg.add("SciMLSensitivity")

Pkg.add("Optim")

Pkg.add("LogDensityProblems")
Pkg.add("LogDensityProblemsAD")
Pkg.add("TransformVariables")
Pkg.add("TransformedLogDensities")
Pkg.add("AbstractMCMC")
Pkg.add("MCMCChains")
Pkg.add("MCMCDiagnosticTools")
Pkg.add("AdvancedHMC")
Pkg.add("DynamicHMC")
Pkg.add("AdaptiveMCMC")

Pkg.resolve()

println(string("Julia ",VERSION,", number of threads: ",Threads.nthreads()))


