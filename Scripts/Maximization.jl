## ==========================================================
##
## File: Maximization.jl
##
## Interface function to optimzation algorithms
##
## creation:          August 6, 2023 -- Peter Reichert
## last modification: August 8, 2023 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ==========================================================


# Load packages:
# ==============

import Statistics
import Random
import Dates

import ForwardDiff
# import Zygote
# import ReverseDiff

import Optim


include("TransPar.jl")


# Optimization interface:
# =======================

# choices for OptAlg:
#   NelderMead    Nelder-Mead algorithm from `optim.jl`
#   BFGS          BFGSo algorithm from `optim.jl`
#   Momentum      Gradient descent algorithm with momentum from `Optimizers.jl`

function Maximization(f::Function, 
                      par_ini::Vector{Float64}, 
                      parnames::Vector{String};
                      Trans         = missing,
                      OptAlg        = "NelderMead",
                      AutoDiff      = ForwardDiff,
                      maxiters::Int = 1000,
                      verbose       = true)

    # transform parameters and define goal function as a function of transformed parameters:
    # (to avoid constrained minimization)
    # --------------------------------------------------------------------------------------

    if ismissing(Trans)
        Trans = Array{TransPar}(undef,length(par_ini))
        for i in 1:length(par_ini)
            Trans[i] = TransParUnbounded()
        end
    end

    par_trans = TransformForward(Trans,par_ini)

    function f_transpar(par_trans)
        if ! all(isfinite.(par_trans)); return Float64(-Inf); end
        par = TransformBackward(Trans,par_trans)
        return f(par) # + CorrectLogPdf(Trans,par_trans)   # no correction for maximization, even if f is a pdf!
    end

    # build goal function for optimization (minimization):
    # ----------------------------------------------------

    function GoalFun(par_trans) 
        return - f_transpar(par_trans)
    end

    # optimize:
    # ---------

    if verbose

        println(string(Dates.now(),": Maximization: starting maximization with algorithm ",OptAlg))
        println(string("                                       ",
                       "number of parameters: ",length(par_ini)))
        println(string("                                       ",
                       "maximum number of iterations: ",maxiters))
        flush(stdout)

    end

    if OptAlg == "NelderMead"

        result = Optim.optimize(GoalFun,par_trans,Optim.NelderMead(),
                                Optim.Options(iterations = maxiters))
        par_trans_end = Optim.minimizer(result)
		iterations    = Optim.iterations(result)
		converged     = Optim.converged(result)

    elseif OptAlg == "LBFGS"

        autodiff = :forward
        if AutoDiff == "Zygote"; autodiff = :zygote; end

        result = Optim.optimize(GoalFun,par_trans,Optim.LBFGS(),
                                Optim.Options(iterations = maxiters),
                                autodiff = autodiff)
        par_trans_end = Optim.minimizer(result)
		iterations    = Optim.iterations(result)
		converged     = Optim.converged(result)

    else

        error(string("*** Maximization: unknown optimization algorithm ",OptAlg))

    end

    par_end = TransformBackward(Trans,par_trans_end)

    if verbose

        println(string(Dates.now(),": Maximization: Maximization completed (algorithm :",OptAlg,")"))
		println("                                       ",
		        "number of iterations performed: ",iterations)
		println("                                       ",
		        "convergence achieved: ",converged)
        flush(stdout)

    end

    # return result:
    # --------------

    return hcat(parnames,par_end)

end

