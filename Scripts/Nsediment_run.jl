## ==============================================================================
##
## File: Nsediment_run.jl
## Model for nitrogen conversion in lake sediments including 14N and 15N isotopes
##
## This script implements the model to be published as:
##
## Alessandra Mazzoli, Peter Reichert, Claudia Frey, Cameron M. Callbeck, 
## Tim J. Paulus, Jakob Zopfi, Moritz F. Lehmann
## "A new comprehensive biogeochemical model for benthic N isotopes"
## To be submitted in winter 2024/2025
##
## Please consult this paper for explanations
##
## creation:          November 26, 2021 -- Peter Reichert
## last modification: January  19, 2025 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ==============================================================================


# command line arguments:
# =======================

# 1: dataset
# 2: parfile
# 3: model
# 4: AutoDiff (currently ForwardDiff or PolyesterForwardDiff)
# 5: OptAlg or MCMCalg (currently NelderMead, LBFGS; AdvancedHMC, DynamicHMC, am, ram, asm, aswam)
# 6: n_samples for MCMC

# Activate, instatiate and load packages:
# =======================================

import Pkg
Pkg.activate(".")
Pkg.instantiate()

import Statistics
import Random

using  DataFrames
import CSV
import DelimitedFiles

using  JLD2

import Plots
Plots.gr()

include("Maximization.jl")
include("BayesianInference.jl")
include("Nsediment.jl")

println(string("Julia ",VERSION,", number of threads: ",Threads.nthreads()))


# Run model:
# ==========


# Initialize parameters, state, and control variables:
# -----------------------------------------------------

# choose which tasks to perform:

readdata        = true
simulate        = true
testgradients   = false
calcposterior   = true
optimize        = false
inferparameters = false
produceplots    = true;  # if Sys.islinux(); global produceplots = false; end
readandplot     = false
predict         = false

onestepden      = false
limtype         = "monod"
# limtype         = "exp"

# directories:

dir_par = "../Parameters"
dir_dat = "../Data"
dir_out = "../Output"

if ! isdir(dir_out); mkdir(dir_out); end

# file version extension:

# versionext = "newbase_exp"
# versionext = "newbase_MinOxNit1FNH4"
# versionext = "newbase"
# versionext = "old"
# versionext = "oldmod"
# versionext = "test"
versionext = ""

# choice of data sets (for optimization and inference only the first one is used)

# datasets  = ["Lucerne_Apr2021"]
# datasets  = ["Baldegg_May2021"]
# datasets  = ["Baldegg_Nov2021"]
# datasets  = ["Sarnen_May2021"]
# datasets  = ["Superior_WM"]
# datasets  = ["Superior_IR"]
# datasets  = ["Skagerrak_S4"]
# datasets  = ["Skagerrak_S6"]
# datasets  = ["Skagerrak_S9"]
# datasets  = ["Bering_MC2"]
# datasets  = ["Bering_MC5"]
# datasets  = ["Bering_MC16"]
# datasets  = ["Bering_MC19"]
# datasets  = ["Bering_MC29"]
# datasets  = ["Bering_MC50"]

datasets = [
            "Lucerne_Apr2021",
            "Baldegg_Mar2021",
            "Baldegg_May2021",
            "Baldegg_Jul2021",
            "Baldegg_Sep2021",
            "Baldegg_Nov2021",
            "Sarnen_May2021",
            "Sarnen_Jul2021",
            "Sarnen_Sep2021",
            "Sarnen_Nov2021",
            "Superior_WM",
            "Superior_IR",
            "Skagerrak_S4",
            "Skagerrak_S6",
            "Skagerrak_S9",
            "Bering_MC2",
            "Bering_MC5",
            "Bering_MC16",
            "Bering_MC19",
            "Bering_MC29",
            # "Bering_MC50",
           ]

datasets = [
            "Lucerne_Apr2021",
            # "Bering_MC5",
            # "Bering_MC19",
            # "Bering_MC29"
           ]
           

if optimize || inferparameters || testgradients; datasets = [datasets[1]]; end
if length(ARGS) > 0; datasets = [ARGS[1]]; end

# model

# model = "N"
model = "N1415"
if length(ARGS) > 2; model = ARGS[3]; end

# choose options for intermediate output:

verbose_loglik  = false
verbose_logpost = true

# choose integration algorithm (overview of options: https://diffeq.sciml.ai/stable/solvers/ode_solve/)

# IntAlg         = OrdinaryDiffEq.Tsit5()
# IntAlg         = OrdinaryDiffEq.BS3()
# IntAlg         = OrdinaryDiffEq.ImplicitEuler()
# IntAlg         = OrdinaryDiffEq.ImplicitMidpoint()
# IntAlg         = OrdinaryDiffEq.Trapezoid()

# IntAlg         = OrdinaryDiffEq.Rosenbrock23()
# IntAlg         = OrdinaryDiffEq.TRBDF2()
# IntAlg         = OrdinaryDiffEq.QNDF()
IntAlg         = OrdinaryDiffEq.FBDF()
# IntAlg         = OrdinaryDiffEq.Hairer4()
# IntAlg         = OrdinaryDiffEq.Rodas3()
# IntAlg         = OrdinaryDiffEq.Rodas5()
# IntAlg         = OrdinaryDiffEq.ROS3P()

# choose automatic differentiation algorithm:

AutoDiff       = "ForwardDiff"
# AutoDiff       = "PolyesterForwardDiff"
# AutoDiff       = "Zygote"
if length(ARGS) > 3; AutoDiff = ARGS[4]; end
chunksize = 11
if AutoDiff == "PolyesterForwardDiff"; println(string("chunksize: ",chunksize)); end


# choose Optimization algorithm:

OptAlg         = "NelderMead"
# OptAlg         = "LBFGS"

# choose Bayesian inference algorithm:

MCMCalg        = "AdvancedHMC"
# MCMCalg        = "DynamicHMC"
# MCMCalg        = "am"
# MCMCalg        = "ram"
# MCMCalg        = "asm"
# MCMCalg        = "aswam"

if length(ARGS) > 4
    if (ARGS[5] == "NelderMead") || (ARGS[5]=="LBFGS")
        OptAlg = ARGS[5]
        optimize = true
        inferparameters = false
    else
        MCMCalg = ARGS[5]
        # optimize = false
        inferparameters = true
    end
end

for dataset in datasets

    println()
    println("dataset ",dataset)

    # parameter file:

    parfile = string(dataset,"_parameters.txt")
    if length(ARGS) > 1; parfile = ARGS[2]; end

    # parameter sample file:

    parsampfile_core = ""  # if not provided, generated automatically below
    # parsampfile_core = "Lucerne_Apr2021_N1415_NLNewton_PolyesterForwardDiff_AdvancedHMC10_100"
    # parsampfile_core = "Lucerne_Apr2021_N1415_NLNewton_ForwardDiff_am_10000"
    # thin_predict = missing
    thin_predict     = 4  # overwrites value from parameter file! Value from parameter file used for thin_predict=missing

    # read model parameters:

    parameters = CSV.read(string(dir_par,"/",parfile),DataFrame;delim='\t',header=true)
    if ! ( ("name" in names(parameters)) & ("value" in names(parameters)) )
    error(string("*** The parameter input file ",parfile," does not contain colums name and value"))
    end
    parnames     = convert(Array{String},parameters[:,"name"])
    par          = parameters[:,"value"]
    parnames_est = String[]
    par_est      = Float64[]
    if "fit" in names(parameters)
        ind = findall(x -> isequal(SubString(x,1,1),"y") ,parameters[:,"fit"])
        if length(ind) > 0
            parnames_est = parnames[ind]
            par_est      = par[ind]
        end
    end

    # extract output time points:

    parnames_required_time = ["t_start","t_end","n_t"]
    
    for i in eachindex(parnames_required_time)
        if ! in(parnames_required_time[i],parnames)
            error(string("*** not all time parameters available: ",join(parnames_required_time,",")))
        end
    end

    t_start = par[findfirst(isequal("t_start"),parnames)]
    t_end   = par[findfirst(isequal("t_end"),parnames)]
    n_t     = round(Int,par[findfirst(isequal("n_t"),parnames)])
    if n_t < 2; n_t = 2; end
    times   = collect(range(t_start,t_end,n_t))

    # extract parameters of the numerical algorithms for integration and sampling:

    parnames_required_num = ["n_cells","f_cellsize",
                            "reltol_int","abstol_int","maxiters_int","dt_ini_int",
                            "maxiters_opt","n_samples_MCMC","n_samples_HMC","thin_MCMC","n_leapfrog_HMC"]

    for i in eachindex(parnames_required_num)
        if ! in(parnames_required_num[i],parnames)
            error(string("*** not all numerics parameters available: ",join(parnames_required_num,",")))
        end
    end

    n_cells    = round(Int,par[findfirst(isequal("n_cells"),parnames)])
    f_cellsize = par[findfirst(isequal("f_cellsize"),parnames)]
    if n_cells < 10; n_cells = 10; end
    if f_cellsize < 0.0; f_cellsize = 1.0; end

    reltol     = par[findfirst(isequal("reltol_int"),parnames)]
    abstol     = par[findfirst(isequal("abstol_int"),parnames)]
    maxiters   = round(Int,par[findfirst(isequal("maxiters_int"),parnames)])
    dt_ini     = par[findfirst(isequal("dt_ini_int"),parnames)]

    maxiters_opt   = round(Int,par[findfirst(isequal("maxiters_opt"),parnames)])

    n_samples_MCMC = round(Int,par[findfirst(isequal("n_samples_MCMC"),parnames)])
    n_samples_HMC  = round(Int,par[findfirst(isequal("n_samples_HMC"),parnames)])
    if (MCMCalg=="AdvancedHMC") || (MCMCalg=="DynamicHMC"); n_samples_MCMC = n_samples_HMC; end
    if length(ARGS) > 5
        n_samples_MCMC = parse(Int,ARGS[6])
        global predict = true  # for batch runs do prediction
        parsampfile_core = ""
    end
    thin_MCMC      = round(Int,par[findfirst(isequal("thin_MCMC"),parnames)])
    if thin_MCMC < 1; thin_MCMC = 1; end
    # n_leapfrog_HMC only applied for :AdvancedHMC: n_leapfrog=0 leads to NUTS, n_leapfrog>0 indicates a fixed number of leapfrog steps
    n_leapfrog_HMC = round(Int,par[findfirst(isequal("n_leapfrog_HMC"),parnames)])
    if n_leapfrog_HMC < 0; n_leapfrog_HMC = 0; end
    step_initial_HMC = missing
    ind_step = findfirst(isequal("step_initial_HMC"),parnames)
    if ! isnothing(ind_step)
    if par[ind_step] >= 0.0; step_initial_HMC = par[ind_step]; end
    end

    # Add integration and autodiff algorithms to version string:
    # ----------------------------------------------------------

    version = string(dataset,"_",model,"_")
    IntAlg_str = String(Symbol(IntAlg))
    i = findfirst(isequal('.'),IntAlg_str); if !isnothing(i); IntAlg_str = SubString(IntAlg_str,i+1); end
    i = findfirst(isequal('{'),IntAlg_str); if !isnothing(i); if i > 1; IntAlg_str = SubString(IntAlg_str,1,i-1); end; end
    i = findfirst(isequal('('),IntAlg_str); if !isnothing(i); if i > 1; IntAlg_str = SubString(IntAlg_str,1,i-1); end; end
    version = string(version,IntAlg_str,"_",AutoDiff)
    if optimize
        version = string(version,"_",OptAlg)
    end
    if inferparameters | readandplot | predict
        version = string(version,"_",String(MCMCalg),
                        if MCMCalg == "AdvancedHMC"; if n_leapfrog_HMC == 0; "NUTS"; else string(n_leapfrog_HMC); end; else ""; end,
                        "_",n_samples_MCMC)
    end
    if length(versionext) > 0; version = string(version,"_",versionext); end
    if length(ARGS) > 6; version = string(version,"_",ARGS[7]); end

    if length(parsampfile_core) == 0; parsampfile_core = version; end


    # Read data:
    # ----------

    obs = []
    if readdata | optimize | inferparameters | calcposterior

        if isfile(string(dir_dat,"/",dataset,".txt"))
            data = CSV.read(string(dir_dat,"/",dataset,".txt"),DataFrame;delim='\t',header=true)
            obs = [data]
        else
            k = 1
            while isfile(string(dir_dat,"/",dataset,"_",k,".txt"))
                data = CSV.read(string(dir_dat,"/",dataset,"_",k,".txt"),DataFrame;delim='\t',header=true)
                obs = vcat(obs,data)
                k = k + 1
            end
        end
        if length(obs) == 0
        error(string("*** data files for dataset ",dataset," not found ***"))
        end

        println()
        println("dataset ",dataset,": ",length(obs)," datasets read:")
        for k in 1:length(obs); println("   ",names(obs[k])); end

    end


    # Simulation:
    # -----------

    if simulate

        println()
        println("dataset ",dataset,": simulation")

        @time states,statenames,fluxes,fluxnames,rates,ratenames = 
                                Nsediment_Run(model,times,vcat(par_est,par),vcat(parnames_est,parnames);
                                              onestepden=onestepden,limtype=limtype,
                                              IntAlg=IntAlg,
                                              n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                                              filename=string(dir_out,"/",version,"_simulation"));

        Nsediment_WritePar(parameters,string(dir_out,"/",version,"_parameters.txt"));

        if produceplots && !ismissing(states)
            Nsediment_Plot(states,statenames;type="allprofiles",
                           obs=obs,filename=string(dir_out,"/",version,"_simconv"))
            Nsediment_Plot(states,statenames;rates=rates,ratenames=ratenames,
                           obs=obs,filename=string(dir_out,"/",version,"_simulation"))
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_simulation_noN2scaling"),
                           max_conc=maximum(states[findall(x->(x!="N2") && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                           plot_d15N=false)
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_simulation_noSO4scaling"),
                           max_conc=maximum(states[findall(x->(x!="SO4"),statenames[1]),:,end]),
                           plot_d15N=false)
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_simulation_noN2andSO4scaling"),
                           max_conc=maximum(states[findall(x->((x!="N2")&&(x!="SO4")) && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                           plot_d15N=false)
        end

    end


    # Set up inference:
    # =================

    if inferparameters | testgradients | optimize | calcposterior

        # Define parameter transformation:
        # --------------------------------

        ParTrans = Array{TransPar}(undef,length(par_est));
        for j = 1:length(par_est)
            if parnames_est[j] == "k_MinAnae"
                ParTrans[j] = TransParInterval(0.001,10.0)
            elseif parnames_est[j][1:min(2,length(parnames_est[j]))] == "k_"
                ParTrans[j] = TransParInterval(1.0,20000.0)
            elseif parnames_est[j][1:min(2,length(parnames_est[j]))] == "K_"
                ParTrans[j] = TransParInterval(0.05,50.0)
            elseif parnames_est[j] == "f_Nit2_Nit1"
                ParTrans[j] = TransParInterval(1.0e-6,20.0)
            elseif parnames_est[j][1:min(5,length(parnames_est[j]))] == "f_Den"
                ParTrans[j] = TransParInterval(1.0e-6,20.0)
            elseif parnames_est[j][1:min(10,length(parnames_est[j]))] == "f_Anam_Den"
                ParTrans[j] = TransParInterval(1.0e-6,10.0)
            elseif parnames_est[j][1:min(2,length(parnames_est[j]))] == "f_"
                ParTrans[j] = TransParInterval(1.0e-6,1.0)
            elseif parnames_est[j][1:min(6,length(parnames_est[j]))] == "gamma_"
                ParTrans[j] = TransParInterval(0.01,2.0)
            elseif parnames_est[j][1:min(2,length(parnames_est[j]))] == "F_"
                ParTrans[j] = TransParInterval(-200.0,0.0)
            elseif parnames_est[j][1:min(9,length(parnames_est[j]))] == "deltaN15_"
                ParTrans[j] = TransParInterval(-20.0,20.0)
            elseif parnames_est[j][1:min(4,length(parnames_est[j]))] == "eps_"
                ParTrans[j] = TransParInterval(-50.0,50.0)
            elseif parnames_est[j] == "a_N2O_Nit1"
                ParTrans[j] = TransParInterval(0.02,2.0)
            elseif parnames_est[j] == "b_N2O_Nit1"
                ParTrans[j] = TransParInterval(0.008,0.8)
            elseif parnames_est[j] == "C_N2O"
                ParTrans[j] = TransParInterval(1.0e-4,0.1)
            elseif parnames_est[j][1:min(2,length(parnames_est[j]))] == "C_"
                ParTrans[j] = TransParInterval(1.0e-4,200.0)
            elseif parnames_est[j][1:min(6,length(parnames_est[j]))] == "sigma_"
                ParTrans[j] = TransParInterval(1.0e-4,10.0)
            elseif parnames_est[j] == "depth_bio"
                ParTrans[j] = TransParInterval(1.0e-4,10.0)
            elseif parnames_est[j] == "D_bio"
                ParTrans[j] = TransParInterval(1.0e-4,10.0)
            else
                # ParTrans[j] = TransParLowerBound(1.0e-4)                        # only positive values allowed
                ParTrans[j] = TransParInterval(1.0e-4,10000.0)                  # only positive values, constrain maximum 
                                                                                # to facilitate optimization and inference
            end
        end

        # Define prior, likelihood, and posterior with only estimated parameter argument:
        # -------------------------------------------------------------------------------

        LogPrior(par_est)  = Nsediment_LogPrior(par_est,parnames_est,par,parnames)

        LogLikeli = Nsediment_Get_LogLikeli(model,times,par,parnames,obs,parnames_est;
                                            onestepden=onestepden,limtype=limtype,
                                            IntAlg=IntAlg,
                                            n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                                            verbose=verbose_loglik)

        LogPosterior = Nsediment_Get_LogPosterior(par_est,LogPrior,LogLikeli,parnames_est;verbose=verbose_logpost)

        if calcposterior

            println()
            println("dataset ",dataset,": calculation of posterior")

            sigdigits = 6

            println()
            @time logposterior = LogPosterior(par_est)
            println(string("Log posterior:                      ",round(logposterior,sigdigits=sigdigits)))

        end

        if testgradients

            println()
            println("dataset ",dataset,": calculation of gradients")

            sigdigits = 6

            # evaluate log prior and its gradient:

            # test log prior:
            println()
            @time logpri = LogPrior(par_est)
            println(string("Log prior:                      ",round(logpri,sigdigits=sigdigits)))
            # test of gradient of log prior:
            println()
            @time grad = ForwardDiff.gradient(LogPrior,par_est)
            println(string("Log prior ForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))
            println()
            grad = similar(par_est)
            @time PolyesterForwardDiff.threaded_gradient!(LogPrior,grad,par_est,ForwardDiff.Chunk(8),
                                                        Val{true}())
            println(string("Log prior PolyesterForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))

            # evaluate log likelihood and its gradient:

            # test log likelihood:
            println()
            @time loglik = LogLikeli(par_est)
            println(string("Log likelihood:                      ",round(loglik,sigdigits=sigdigits)))
            # test of gradient of log likelihood:
            println()
            @time grad = ForwardDiff.gradient(LogLikeli,par_est)
            println(string("Log likelihood ForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))
            println()
            grad = similar(par_est)
            @time PolyesterForwardDiff.threaded_gradient!(LogLikeli,grad,par_est,ForwardDiff.Chunk(8),
                                                        Val{true}())
            println(string("Log likelihood PolyesterForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))

            # evaluate log posterior and its gradient:

            # test log posterior:
            println()
            @time logposterior = LogPosterior(par_est)
            println(string("Log posterior:                      ",round(logposterior,sigdigits=sigdigits)))
            # test of gradient of log posterior:
            println()
            @time grad = ForwardDiff.gradient(LogPosterior,par_est)
            println(string("Log posterior ForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))
            println()
            grad = similar(par_est)
            @time PolyesterForwardDiff.threaded_gradient!(LogPosterior,grad,par_est,ForwardDiff.Chunk(8),
                                                        Val{true}())
            println(string("Log posterior PolyesterForwardDiff.gradient: ",
                        string.(parnames_est,"=",round.(grad,sigdigits=sigdigits))))

        end


        # Posterior maximization:
        # =======================

        if optimize & (length(par_est) > 0)
    
            println()
            println("dataset ",dataset,": posterior maximization")

            println("initial parameter values:")
            display(hcat(parnames_est,par_est))
            println(string("initial log prior value:      ",LogPrior(par_est)))
            println(string("initial log likelihood value: ",LogLikeli(par_est)))
            println(string("initial log posterior value:  ",LogPosterior(par_est)))

            result  = Maximization(LogPosterior,par_est,parnames_est;
                                Trans    = ParTrans,
                                AutoDiff = AutoDiff,
                                OptAlg   = OptAlg,
                                maxiters = maxiters_opt)

            par_est = convert.(Float64,result[:,2])
        
            Nsediment_WritePar(parameters,string(dir_out,"/",version,"_maxpost_parameters.txt"),par_est=par_est,parnames_est=parnames_est);

            println("final parameter values:")
            display(result)
            println(string("final log prior value:     ",LogPrior(par_est)))
            println(string("final log likelihood value:",LogLikeli(par_est)))
            println(string("final log posterior value: ",LogPosterior(par_est)))

            if produceplots

                @time states,statenames,fluxes,fluxnames,rates,ratenames = 
                                        Nsediment_Run(model,times,vcat(par_est,par),vcat(parnames_est,parnames);
                                                      onestepden=onestepden,limtype=limtype,
                                                      IntAlg=IntAlg,
                                                      n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                                                      filename=string(dir_out,"/",version,"_maxpost_simulation"));

                if !ismissing(states)

                    Nsediment_Plot(states,statenames;type="allprofiles",obs=obs,filename=string(dir_out,"/",version,"_maxpost_simconv"))
                    Nsediment_Plot(states,statenames;rates=rates,ratenames=ratenames,
                                   obs=obs,filename=string(dir_out,"/",version,"_maxpost_simulation"))
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_maxpost_simulation_noN2scaling"),
                                   max_conc=maximum(states[findall(x->(x!="N2") && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                                   plot_d15N=false)
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_maxpost_simulation_noSO4scaling"),
                                   max_conc=maximum(states[findall(x->(x!="SO4"),statenames[1]),:,end]),
                                   plot_d15N=false)
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_maxpost_simulation_noN2andSO4scaling"),
                                   max_conc=maximum(states[findall(x->((x!="N2")&&(x!="SO4")) && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                                   plot_d15N=false)

                end

            end
        
        end


        # Perform inference:
        # ==================

        if inferparameters & (length(par_est) > 0)

            println()
            println("dataset ",dataset,": Bayesian inference")

            # write marginal prior densities:

            Nsediment_WriteMargPriorDensities(par_est,parnames_est,par,parnames,string(dir_out,"/",version));

            # sample from posterior:

            sample = BayesianInference(LogPosterior,par_est,parnames_est;
                                    Trans        = ParTrans,
                                    AutoDiff     = AutoDiff,
                                    MCMCalg      = MCMCalg,
                                    n_samples    = n_samples_MCMC,
                                    thin         = thin_MCMC,
                                    n_leapfrog   = n_leapfrog_HMC,
                                    initial_step = step_initial_HMC,
                                    chunksize    = chunksize)

            # save chains:

            @save string(dir_out,"/",version,"_markovchain.jld2") sample par parnames par_est parnames_est
            #@load string(dir_out,"/",version,"_markovchain.jld2") sample par parnames par_est parnames_est

            # CSV.write(string(dir_out,"/",version,"_markovchain.txt"),DataFrame(sample),delim='\t')
            CSV.write(string(dir_out,"/",version,"_markovchain.txt"),
                    Tables.table(Array(sample);header=parnames_est),
                    delim='\t')
    
            par_est = Statistics.mean(sample)[:,2]
            
            Nsediment_WritePar(parameters,string(dir_out,"/",version,"_meanpost_parameters.txt"),par_est=par_est,parnames_est=parnames_est);

            # plot chains:

            if produceplots
                Plots.plot(sample;seriestype=(:traceplot,:histogram))
                Plots.savefig(string(dir_out,"/",version,"_markovchain.pdf"))

                @time states,statenames,fluxes,fluxnames,rates,ratenames = 
                                        Nsediment_Run(model,times,vcat(par_est,par),vcat(parnames_est,parnames);
                                                      onestepden=onestepden,limtype=limtype,
                                                      IntAlg=IntAlg,
                                                      n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                                                      filename=string(dir_out,"/",version,"_meanpost_simulation"));

                if !ismissing(states)

                    Nsediment_Plot(states,statenames;type="allprofiles",obs=obs,filename=string(dir_out,"/",version,"_meanpost_simconv"))
                    Nsediment_Plot(states,statenames;rates=rates,ratenames=ratenames,
                                   obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation"))
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noN2scaling"),
                                   max_conc=maximum(states[findall(x->(x!="N2") && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                                   plot_d15N=false)
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noSO4scaling"),
                                   max_conc=maximum(states[findall(x->(x!="SO4"),statenames[1]),:,end]),
                                   plot_d15N=false)
                    Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noN2andSO4scaling"),
                                   max_conc=maximum(states[findall(x->((x!="N2")&&(x!="SO4")) && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                                   plot_d15N=false)

                end

            end

        end

    end


    # Read results and plot:
    # ======================

    if readandplot

        @load string(dir_out,"/",version,"_markovchain.jld2") sample par parnames par_est parnames_est

        Plots.plot(sample;seriestype=(:traceplot,:histogram))
        Plots.savefig(string(dir_out,"/",version,"_markovchain.pdf"))

        res = Statistics.mean(sample)
        parnames_est = String.(res[:,1])
        par_est      = res[:,2]

        states,statenames,fluxes,fluxnames,rates,ratenames = 
                            Nsediment_Run(model,times,vcat(par_est,par),vcat(parnames_est,parnames);
                                          onestepden=onestepden,limtype=limtype,
                                          IntAlg=IntAlg,
                                          n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                                          filename=string(dir_out,"/",version,"_meanpost_simulation"));

        if produceplots && !ismissing(states)
            Nsediment_Plot(states,statenames;type="allprofiles",obs=obs,filename=string(dir_out,"/",version,"_meanpost_simconv"))
            Nsediment_Plot(states,statenames;rates=rates,ratenames=ratenames,
                           obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation"))
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noN2scaling"),
                           max_conc=maximum(states[findall(x->(x!="N2") && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                           plot_d15N=false)
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noSO4scaling"),
                           max_conc=maximum(states[findall(x->(x!="SO4"),statenames[1]),:,end]),
                           plot_d15N=false)
            Nsediment_Plot(states,statenames;obs=obs,filename=string(dir_out,"/",version,"_meanpost_simulation_noN2andSO4scaling"),
                           max_conc=maximum(states[findall(x->((x!="N2")&&(x!="SO4")) && (x[1:min(3,length(x))]!="N2_"),statenames[1]),:,end]),
                           plot_d15N=false)
        end

    end


    # Propagate parameter sampel through the model:
    # =============================================

    if predict

        println()
        println("dataset ",dataset,": calculation of model predictions")

        Nsediment_Predict(model,[t_start,t_end],
                          string(dir_out,"/",parsampfile_core,"_meanpost_parameters.txt"),
                          string(dir_out,"/",parsampfile_core,"_markovchain.txt"),
                          string(dir_out,"/",parsampfile_core);
                          onestepden=onestepden,limtype=limtype,
                          IntAlg=IntAlg,
                          n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters,
                          thin=thin_predict)

    end

end
