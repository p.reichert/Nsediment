#!/bin/bash
julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters.txt N NelderMead &> ../Output/Lucerne_Apr2021_N_FBDF_ForwardDiff_NelderMead.log &
julia Nsediment_run.jl Lucerne_Apr2021 Lucerne_Apr2021_parameters.txt N LBFGS &> ../Output/Lucerne_Apr2021_N_FBDF_ForwardDiff_LBFGS.log &
