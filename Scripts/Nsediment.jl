## ==============================================================================
##
## File: Nsediment.jl
##
## Model for nitrogen conversion in lake sediments including 14N and 15N isotopes
##
## This script implements the model to be published as:
##
## Alessandra Mazzoli, Peter Reichert, Claudia Frey, Cameron M. Callbeck, 
## Tim J. Paulus, Jakob Zopfi, Moritz F. Lehmann
## "A new comprehensive biogeochemical model for benthic N isotopes"
## To be submitted in winter 2024/2025
##
## Please consult this paper for explanations
##
## creation:          November 26, 2021 -- Peter Reichert
## last modification: January 7  , 2025 -- Peter Reichert
##
## contact:           peter.reichert@emeriti.eawag.ch
##
## ==============================================================================


# Activate, instatiate and load packages:
# =======================================

import OrdinaryDiffEq
import SciMLSensitivity
import DistributionsAD

include("IntPol.jl")


# Global parameter:
# =================

R_N15N14_std = 0.0036765    # https://doi.org/10.1002/rcm.8890


# Conversion functions:
# =====================

function calc_isotope_delta(C1,C2;R_std=0.0036765)

    delta = NaN
    if C1 > 1.0e-2; delta = ( C2 / C1 / R_std - 1.0 ) * 1000.0; end
    return delta

end

function calc_isotope_delta(C1,C2,C3;R_std=0.0036765)

    delta = NaN
    if C1 > 1.0e-2; delta = ( (C2+2.0*C3) / (2.0*C1+C2) / R_std - 1.0 ) * 1000.0; end
    return delta

end

function calc_isotope_C1C2_fromCtot(Ctot,delta;R_std=0.0036765)

    r = ( delta/1000.0 + 1.0 ) * R_std
    return Ctot/(1.0+r) .* [1.0,r]

end

# (In the case of nitrogen), the following function estimates 14N14N, 14N15N, 15N15N
# concentrations from deltaN15 and Ctot. This assumes a random distribution of the isotopes
# across the compounds 14N14N : 14N15N : 15N15N = 1 : 2r : r^2,
# see also https://doi.org/10.4141/cjss87-075
#
# r = 15N/14N relates to a and b in equation (4) in this paper by a=1/(1+r) and b=r/(1+r)

function calc_isotope_C1C2C3_fromCtot(Ctot,delta;R_std=0.0036765)

    r = ( delta/1000.0 + 1.0 ) * R_std
    return Ctot/(1.0+2.0*r+r^2) .* [1.0,2.0*r,r^2]

end

#=

# example:

N14 = 30.0
N15 = 0.112
deltaN15 = calc_isotope_delta(N14,N15;R_std=R_N15N14_std); println("deltaN15 = ",deltaN15)
C1C2 = calc_isotope_C1C2_fromCtot(N14+N15,deltaN15;R_std=R_N15N14_std); println("[C1,C2] = ",C1C2)


N1414 = 15.0 - 0.5*0.112
N1415 = 0.112
N1515 = 0.0
deltaN15 = calc_isotope_delta(N1414,N1415,N1515;R_std=R_N15N14_std); println("deltaN15 = ",deltaN15)
# asssumed standard ratios C1/C2/C3:
C1C2C3 = calc_isotope_C1C2C3_fromCtot(N1414+N1415+N1515,deltaN15;R_std=R_N15N14_std); println("[C1,C2,C3] = ",C1C2C3)

=#


 # Limitation and inhibition functions:
 # ------------------------------------

function Nsediment_Lim(c;type="monod")

    if type == "monod"
        return c/(1.0+c)
    elseif type == "exp"
        return 1.0 - exp(-log(2.0)*c)
    else
        println(string("*** Nsediment_Lim: unknonw limitation type: ",type))
        return NaN
    end

end

function Nsediment_Inh(c;type="monod")

    if type == "monod"
        return 1.0/(1.0+c)
    elseif type == "exp"
        return exp(-log(2.0)*c)
    else
        println(string("*** Nsediment_Inh: unknonw inhibition type: ",type))
        return NaN
    end

end



# Model simulation function:
# ==========================

function Nsediment_Run(model,times,par,parnames;
                       onestepden=false,limtype="monod",
                       IntAlg=OrdinaryDiffEq.Tsit5(),
                       n_cells=50,f_cellsize=1.0,dt_ini=0.1,reltol=1.0e-4,abstol=1.0e-4,maxiters=100000,
                       filename=missing,getrates=true)

    statenames_required_N,statenames_required_N1415 =  Nsediment_GetConcNames()   
    
    parnames_required_N   = [
        "D_bio","depth_bio",                                                         # bioturbation
        "a_tort","m_tort",                                                           # tortuosity parameters
        "k_MinOx","K_O2_MinOx","gamma_NH4_MinOx",                                    # Oxic mineralization
        "k_MinSulfRed","K_O2_MinSulfRed","K_NO3_MinSulfRed","K_SO4_MinSulfRed",      # Mineralization by sulfate reduction
            "gamma_NH4_MinSulfRed",
        "k_MinAnae","K_O2_MinAnae","K_NO3_MinAnae",                                  # Anaerobic mineralization
        "k_Nit1","f_Nit2_Nit1","K_O2_Nit1","K_O2_Nit2","K_NH4_Nit1","K_NO2_Nit2",         # Nitrification
            "a_N2O_Nit1","b_N2O_Nit1",
        "K_NH4_Anam","K_NO2_Anam","K_O2_Anam","f_Anam_side",                         # Anammox
            "K_O2_DNRA1","K_O2_DNRA2","K_NO3_DNRA1","K_NO2_DNRA2",                   # DNRA
            "gamma_NH4_DNRA1","gamma_NH4_DNRA2",
        "F_NH4","F_NO2","F_NO3","F_N2O","F_N2","F_O2"]
    if onestepden                                                                    # Denitrification
        parnames_required_N = vcat(parnames_required_N,
            ["k_Den","K_O2_Den","K_NO3_Den","gamma_NH4_Den",
            "f_Anam_Den",
            "f_DNRA1_Den","f_DNRA2_Den"])
    else
        parnames_required_N = vcat(parnames_required_N,
            ["k_Den1","f_Den2_Den1","f_Den3_Den1","K_O2_Den1","K_O2_Den2","K_O2_Den3",
            "K_NO3_Den1","K_NO2_Den2","K_N2O_Den3",
            "gamma_NH4_Den1","gamma_NH4_Den2","gamma_NH4_Den3",
            "f_Anam_Den2",
            "f_DNRA1_Den1","f_DNRA2_Den2"])
        end
    parnames_required_N1415  = ["deltaN15_OM",
                                "eps_Nit1_NO2","eps_Nit1_N2O","eps_Nit2",
                                "eps_Anam_NH4","eps_Anam_NO2","eps_Anam_side",
                                "eps_DNRA1","eps_DNRA2",
                                "deltaN15_F_NH4","deltaN15_F_NO2","deltaN15_F_NO3","deltaN15_F_N2O","deltaN15_F_N2"]
    if onestepden                                                                # Denitrification
        parnames_required_N1415 = vcat(parnames_required_N1415,
            ["eps_Den"])
    else
        parnames_required_N1415 = vcat(parnames_required_N1415,
            ["eps_Den1","eps_Den2","eps_Den3"])
    end

    # Processes:
    # ----------

    # Oxic Mineralization

    # C106H263O110N16P + 106 O2 → 106 HCO3(-) + 16 NH4(+) + HPO4(2−) + 92 H(+)

    # gamma_NH4_MinOx = 16/106 = 0.151  [NH4(+) released from OM per O2 consummed]

    function MineralizationOx!(rates,conc,par)

        # r_MinOx = par[ind_N.k_MinOx] *
        #           conc[ind_state.O2]/(par[ind_N.K_O2_MinOx]+conc[ind_state.O2])

        r_MinOx = par[ind_N.k_MinOx] *
                  Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_MinOx];type=limtype)

        rates[ind_state.O2]  +=                            - r_MinOx
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_MinOx] * r_MinOx

    end

    function MineralizationOx_N1415!(rates,conc,par)

        # r_MinOx = par[ind_N.k_MinOx] *
        #           conc[ind_state.O2]/(par[ind_N.K_O2_MinOx]+conc[ind_state.O2])

        r_MinOx = par[ind_N.k_MinOx] *
                  Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_MinOx];type=limtype)

        R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

        rates[ind_state.O2]      +=                                      - r_MinOx
        rates[ind_state.NH4_N14] += par[ind_N.gamma_NH4_MinOx] * R_OM[1] * r_MinOx
        rates[ind_state.NH4_N15] += par[ind_N.gamma_NH4_MinOx] * R_OM[2] * r_MinOx

    end

    # Denitrification as a three step process:

    # Den1: 

    # 2 C106H263O110N16P + 424 NO3(-) → 212 HCO3(-) + 32 NH4(+) + 2 HPO4(2−) + 424 NO2(−) + 184 H(+)
    
    # gamma_NH4_Den1 = 32/424 = 0.0755  [NH4(+) released from OM per NO3(-) consumed]

    # Den2:

    # 2 C106H263O110N16P + 424 NO2(-) + 240 H(+) → 212 HCO3(-) + 32 NH4(+) + 2 HPO4(2−) + 212 N2O + 212 H2O
    
    # gamma_NH4_Den2 = 32/424 = 0.0755  [NH4(+) released from OM per NO2(-) consumed]

    # Den3:

    # C106H263O110N16P + 212 N2O → 106 HCO3(-) + 16 NH4(+) + HPO4(2−) + 212 N2 + 92 H(+)

    # gamma_NH4_Den3 = 16/212 = 0.0755  [NH4(+) released from OM per N2O consumed]

    function Denitrification_1!(rates,conc,par)

        # r_Den1 = par[ind_N.k_Den1] *
        #          conc[ind_state.NO3]/(par[ind_N.K_NO3_Den1]+conc[ind_state.NO3]) *
        #          par[ind_N.K_O2_Den1]/(par[ind_N.K_O2_Den1]+conc[ind_state.O2]) 

        r_Den1 = par[ind_N.k_Den1] *
                 Nsediment_Lim(conc[ind_state.NO3]/par[ind_N.K_NO3_Den1];type=limtype) *
                 Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den1];type=limtype) 

        rates[ind_state.NO3] += - r_Den1
        rates[ind_state.NO2] +=   r_Den1
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_Den1] * r_Den1

    end

    function Denitrification_2!(rates,conc,par)

        # r_Den2 = par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1] *
        #          conc[ind_state.NO2]^2/(par[ind_N.K_NO2_Den2]+conc[ind_state.NO2])^2 *
        #          par[ind_N.K_O2_Den2]/(par[ind_N.K_O2_Den2]+conc[ind_state.O2]) 

        r_Den2 = par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1] *
                 Nsediment_Lim(conc[ind_state.NO2]/par[ind_N.K_NO2_Den2];type=limtype)^2 *
                 Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den2];type=limtype) 

        rates[ind_state.NO2] += - 2.0 * r_Den2
        rates[ind_state.N2O] +=         r_Den2
        rates[ind_state.NH4] +=   2.0 * par[ind_N.gamma_NH4_Den2] * r_Den2

    end

    function Denitrification_3!(rates,conc,par)

        # r_Den3 = par[ind_N.f_Den3_Den1] * par[ind_N.k_Den1] *
        #          conc[ind_state.N2O]/(par[ind_N.K_N2O_Den3]+conc[ind_state.N2O]) *
        #          par[ind_N.K_O2_Den3]/(par[ind_N.K_O2_Den3]+conc[ind_state.O2]) 

        r_Den3 = par[ind_N.f_Den3_Den1] * par[ind_N.k_Den1] *
                 Nsediment_Lim(conc[ind_state.N2O]/par[ind_N.K_N2O_Den3];type=limtype) *
                 Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den3];type=limtype) 

        rates[ind_state.N2O] += - r_Den3
        rates[ind_state.N2]  +=   r_Den3
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_Den3] * r_Den3

    end

    function Denitrification_N1415_1!(rates,conc,par)

        # r_Den1_part = par[ind_N.k_Den1] *
        #               1.0/(par[ind_N.K_NO3_Den1]+conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15]) *
        #               par[ind_N.K_O2_Den1]/(par[ind_N.K_O2_Den1]+conc[ind_state.O2])

        # r_Den1_N14   =       r_Den1_part * conc[ind_state.NO3_N14]
        # r_Den1_N15   =       r_Den1_part * conc[ind_state.NO3_N15] * (1.0-0.001*par[ind_N1415.eps_Den1])

        if conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15] != 0.0

            r_Den1_part = par[ind_N.k_Den1] *
                          Nsediment_Lim((conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])/par[ind_N.K_NO3_Den1]) *
                          Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den1];type=limtype)

            r_Den1_N14  = r_Den1_part * conc[ind_state.NO3_N14]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])
            r_Den1_N15  = r_Den1_part * conc[ind_state.NO3_N15]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15]) * 
                          (1.0-0.001*par[ind_N1415.eps_Den1])

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.NO3_N14]   += - r_Den1_N14
            rates[ind_state.NO3_N15]   += - r_Den1_N15
            rates[ind_state.NO2_N14]   +=   r_Den1_N14
            rates[ind_state.NO2_N15]   +=   r_Den1_N15
            rates[ind_state.NH4_N14]   += par[ind_N.gamma_NH4_Den1] * R_OM[1] * (r_Den1_N14+r_Den1_N15)
            rates[ind_state.NH4_N15]   += par[ind_N.gamma_NH4_Den1] * R_OM[2] * (r_Den1_N14+r_Den1_N15)

        end

    end

    function Denitrification_N1415_2!(rates,conc,par)

        # r_Den2_part = par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1] *
        #               1.0/(par[ind_N.K_NO2_Den2]+conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])^2 *
        #               par[ind_N.K_O2_Den2]/(par[ind_N.K_O2_Den2]+conc[ind_state.O2])

        # r_Den2_N1414 =       r_Den2_part * conc[ind_state.NO2_N14] * conc[ind_state.NO2_N14]
        # r_Den2_N1415 = 2.0 * r_Den2_part * conc[ind_state.NO2_N14] * conc[ind_state.NO2_N15] * (1.0-0.001*par[ind_N1415.eps_Den2])
        # r_Den2_N1515 =       r_Den2_part * conc[ind_state.NO2_N15] * conc[ind_state.NO2_N15] * (1.0-0.001*par[ind_N1415.eps_Den2])^2

        if conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15] != 0.0

            r_Den2_part = par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1] *
                          Nsediment_Lim((conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])/par[ind_N.K_NO2_Den2];type=limtype)^2 *
                          Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den2];type=limtype)

            r_Den2_N1414 =       r_Den2_part * conc[ind_state.NO2_N14]*conc[ind_state.NO2_N14]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])^2
            r_Den2_N1415 = 2.0 * r_Den2_part * conc[ind_state.NO2_N14]*conc[ind_state.NO2_N15]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])^2 * 
                                 (1.0-0.001*par[ind_N1415.eps_Den2])
            r_Den2_N1515 =       r_Den2_part * conc[ind_state.NO2_N15]*conc[ind_state.NO2_N15]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])^2 * 
                                 (1.0-0.001*par[ind_N1415.eps_Den2])^2

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.NO2_N14]   += - 2.0 * r_Den2_N1414 -       r_Den2_N1415
            rates[ind_state.NO2_N15]   += -       r_Den2_N1415 - 2.0 * r_Den2_N1515
            rates[ind_state.N2O_N1414] +=         r_Den2_N1414
            rates[ind_state.N2O_N1415] +=         r_Den2_N1415
            rates[ind_state.N2O_N1515] +=         r_Den2_N1515
            rates[ind_state.NH4_N14]   += 2.0 * par[ind_N.gamma_NH4_Den2] * R_OM[1] * (r_Den2_N1414+r_Den2_N1415+r_Den2_N1515)
            rates[ind_state.NH4_N15]   += 2.0 * par[ind_N.gamma_NH4_Den2] * R_OM[2] * (r_Den2_N1414+r_Den2_N1415+r_Den2_N1515)

        end

    end

    function Denitrification_N1415_3!(rates,conc,par)

        # r_Den3_part = par[ind_N.f_Den3_Den1] * par[ind_N.k_Den1] *
        #               1.0/(par[ind_N.K_N2O_Den3]+conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515]) *
        #               par[ind_N.K_O2_Den3]/(par[ind_N.K_O2_Den3]+conc[ind_state.O2])

        # r_Den3_N1414 = r_Den3_part * conc[ind_state.N2O_N1414]
        # r_Den3_N1415 = r_Den3_part * conc[ind_state.N2O_N1415] * (1.0-0.001*par[ind_N1415.eps_Den3])
        # r_Den3_N1515 = r_Den3_part * conc[ind_state.N2O_N1515] * (1.0-0.001*par[ind_N1415.eps_Den3])

        if conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515] != 0.0

            r_Den3_part = par[ind_N.f_Den3_Den1] * par[ind_N.k_Den1] *
                          Nsediment_Lim((conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515])/par[ind_N.K_N2O_Den3];type=limtype) *
                          Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den3];type=limtype)

            r_Den3_N1414 = r_Den3_part * conc[ind_state.N2O_N1414]/(conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515])
            r_Den3_N1415 = r_Den3_part * conc[ind_state.N2O_N1415]/(conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515]) *
                           (1.0-0.001*par[ind_N1415.eps_Den3])
            r_Den3_N1515 = r_Den3_part * conc[ind_state.N2O_N1515]/(conc[ind_state.N2O_N1414]+conc[ind_state.N2O_N1415]+conc[ind_state.N2O_N1515]) * 
                           (1.0-0.001*par[ind_N1415.eps_Den3])

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.N2O_N1414] += - r_Den3_N1414
            rates[ind_state.N2O_N1415] += - r_Den3_N1415
            rates[ind_state.N2O_N1515] += - r_Den3_N1515
            rates[ind_state.N2_N1414]  +=   r_Den3_N1414
            rates[ind_state.N2_N1415]  +=   r_Den3_N1415
            rates[ind_state.N2_N1515]  +=   r_Den3_N1515
            rates[ind_state.NH4_N14]   += par[ind_N.gamma_NH4_Den3] * R_OM[1] * (r_Den3_N1414+r_Den3_N1415+r_Den3_N1515)
            rates[ind_state.NH4_N15]   += par[ind_N.gamma_NH4_Den3] * R_OM[2] * (r_Den3_N1414+r_Den3_N1415+r_Den3_N1515)

        end

    end

    # Denitrification as a one step process:

    # 5 C106H263O110N16P + 424 NO3(-) -> 530 HCO3(-) + 80 NH4(+) + 5 HPO4(2-) + 212 N2 + 36 H(+) + 212 H2O

    # gamma_NH4_Den = 80/424 = 0.189  [NH4(+) released from OM per NO3(-) consumed]

    # [ gamma_NH4_Den = gamma_NH4_Den1 + gamma_NH4_Den2 + 0.5 gamma_NH4_Den3 ]

    function Denitrification!(rates,conc,par)

        # r_Den = par[ind_N.k_Den] *
        #         conc[ind_state.NO3]^2/(par[ind_N.K_NO3_Den]+conc[ind_state.NO3])^2 *
        #         par[ind_N.K_O2_Den]/(par[ind_N.K_O2_Den]+conc[ind_state.O2]) 

        r_Den = par[ind_N.k_Den] *
                Nsediment_Lim(conc[ind_state.NO3]/par[ind_N.K_NO3_Den];type=limtype)^2 *
                Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den];type=limtype) 

        rates[ind_state.NO3] +=     - r_Den
        rates[ind_state.N2]  += 0.5 * r_Den
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_Den] * r_Den

    end

    function Denitrification_N1415!(rates,conc,par)

        # r_Den_part = par[ind_N.k_Den] *
        #              1.0/(par[ind_N.K_NO3_Den]+conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])^2 *
        #              par[ind_N.K_O2_Den]/(par[ind_N.K_O2_Den]+conc[ind_state.O2])

        # r_Den_N1414 =       r_Den_part * conc[ind_state.NO3_N14] * conc[ind_state.NO3_N14]
        # r_Den_N1415 = 2.0 * r_Den_part * conc[ind_state.NO3_N14] * conc[ind_state.NO3_N15] * (1.0-0.001*par[ind_N1415.eps_Den])
        # r_Den_N1515 =       r_Den_part * conc[ind_state.NO3_N15] * conc[ind_state.NO3_N15] * (1.0-0.001*par[ind_N1415.eps_Den])^2

        if conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15] != 0.0

            r_Den_part = par[ind_N.k_Den] *
                         Nsediment_Lim((conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])/par[ind_N.K_NO3_Den];type=limtype)^2 *
                         Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Den];type=limtype)

            r_Den_N1414 =       r_Den_part * conc[ind_state.NO3_N14]*conc[ind_state.NO3_N14]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])^2
            r_Den_N1415 = 2.0 * r_Den_part * conc[ind_state.NO3_N14]*conc[ind_state.NO3_N15]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])^2 * 
                                (1.0-0.001*par[ind_N1415.eps_Den])
            r_Den_N1515 =       r_Den_part * conc[ind_state.NO3_N15]*conc[ind_state.NO3_N15]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])^2 * 
                                (1.0-0.001*par[ind_N1415.eps_Den])^2

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.NO3_N14]  += - 1.0 * r_Den_N1414 - 0.5 * r_Den_N1415
            rates[ind_state.NO3_N15]  += - 0.5 * r_Den_N1415 - 1.0 * r_Den_N1515
            rates[ind_state.N2_N1414] +=   0.5 * r_Den_N1414
            rates[ind_state.N2_N1415] +=   0.5 * r_Den_N1415
            rates[ind_state.N2_N1515] +=   0.5 * r_Den_N1515
            rates[ind_state.NH4_N14]  += par[ind_N.gamma_NH4_Den] * R_OM[1] * (r_Den_N1414+r_Den_N1415+r_Den_N1515)
            rates[ind_state.NH4_N15]  += par[ind_N.gamma_NH4_Den] * R_OM[2] * (r_Den_N1414+r_Den_N1415+r_Den_N1515)

        end

    end

    # Mineralization by Sulfate Reduction

    # C106H263O110N16P + 53 SO4(2-) + 15 H(+) → 106 HCO3(-) + 16 NH4(+) + HPO4(2)- + 53 H2S

    # gamma_NH4_SulfRed = 16/53 = 0.302  [NH4(+) released from OM per SO4(2-) consumed]

    function MineralizationSulfRed!(rates,conc,par)

        # r_MinSulfRed = par[ind_N.k_MinSulfRed] *
        #                par[ind_N.K_O2_MinSulfRed]/(par[ind_N.K_O2_MinSulfRed]+conc[ind_state.O2]) *
        #                par[ind_N.K_NO3_MinSulfRed]/(par[ind_N.K_NO3_MinSulfRed]+conc[ind_state.NO3]) *
        #                conc[ind_state.SO4]/(par[ind_N.K_SO4_MinSulfRed]+conc[ind_state.SO4])

        r_MinSulfRed = par[ind_N.k_MinSulfRed] *
                       Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_MinSulfRed];type=limtype) *
                       Nsediment_Inh(conc[ind_state.NO3]/par[ind_N.K_NO3_MinSulfRed];type=limtype) *
                       Nsediment_Lim(conc[ind_state.SO4]/par[ind_N.K_SO4_MinSulfRed];type=limtype)

        rates[ind_state.SO4] +=                                 - r_MinSulfRed
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_MinSulfRed] * r_MinSulfRed

    end

    function MineralizationSulfRed_N1415!(rates,conc,par)

        # r_MinSulfRed = par[ind_N.k_MinSulfRed] *
        #                par[ind_N.K_O2_MinSulfRed]/(par[ind_N.K_O2_MinSulfRed]+conc[ind_state.O2]) *
        #                par[ind_N.K_NO3_MinSulfRed]/(par[ind_N.K_NO3_MinSulfRed]+conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15]) *
        #                conc[ind_state.SO4]/(par[ind_N.K_SO4_MinSulfRed]+conc[ind_state.SO4])

        r_MinSulfRed = par[ind_N.k_MinSulfRed] *
                       Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_MinSulfRed];type=limtype) *
                       Nsediment_Inh((conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])/par[ind_N.K_NO3_MinSulfRed];type=limtype) *
                       Nsediment_Lim(conc[ind_state.SO4]/par[ind_N.K_SO4_MinSulfRed];type=limtype)

        R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

        rates[ind_state.SO4]     +=                                           - r_MinSulfRed
        rates[ind_state.NH4_N14] += par[ind_N.gamma_NH4_MinSulfRed] * R_OM[1] * r_MinSulfRed
        rates[ind_state.NH4_N15] += par[ind_N.gamma_NH4_MinSulfRed] * R_OM[2] * r_MinSulfRed

    end

    # Anaerobic Mineralization other than by sulfate reduction

    # C106H263O110N16P + a X → b HCO3(-) + 16 NH4(+) + c HPO4(2-) + d H(+) + e Y

    function MineralizationAnae!(rates,conc,par)

        # r_MinAnae = par[ind_N.k_MinAnae] *
        #             par[ind_N.K_O2_MinAnae]/(par[ind_N.K_O2_MinAnae]+conc[ind_state.O2]) *
        #             par[ind_N.K_NO3_MinAnae]/(par[ind_N.K_NO3_MinAnae]+conc[ind_state.NO3])

        r_MinAnae = par[ind_N.k_MinAnae] *
                    Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_MinAnae];type=limtype) *
                    Nsediment_Inh(conc[ind_state.NO3]/par[ind_N.K_NO3_MinAnae];type=limtype)

        rates[ind_state.NH4]  += r_MinAnae

    end

    function MineralizationAnae_N1415!(rates,conc,par)

        # r_MinAnae = par[ind_N.k_MinAnae] *
        #             par[ind_N.K_O2_MinAnae]/(par[ind_N.K_O2_MinAnae]+conc[ind_state.O2]) *
        #             par[ind_N.K_NO3_MinAnae]/(par[ind_N.K_NO3_MinAnae]+conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])

        r_MinAnae = par[ind_N.k_MinAnae] *
                    Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_MinAnae];type=limtype) *
                    Nsediment_Inh((conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])/par[ind_N.K_NO3_MinAnae];type=limtype)

        R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

        rates[ind_state.NH4_N14] += R_OM[1] * r_MinAnae
        rates[ind_state.NH4_N15] += R_OM[2] * r_MinAnae

    end

    # Nitrification:  NH4(+) + 1.5 O2 →     NO2(-) + 2 H(+) +     H2O
    #                 NH4(+) +     O2 → 0.5 N2O    +   H(+) + 1.5 H2O
    #                 NO2(-) + 0.5 O2 →     NO3(-)

    function Nitrification_1!(rates,conc,par)

        f_N2O_Nit1 = par[ind_N.b_N2O_Nit1]*par[ind_N.a_N2O_Nit1]/(conc[ind_state.O2]+par[ind_N.a_N2O_Nit1])
        # Ji et al. 2018 https://doi.org/10.1029/2018GB005887  (equation modified to be bound to the interval [0,1])

        # r_Nit1a = par[ind_N.k_Nit1] * (1.0-f_N2O_Nit1) *
        #           conc[ind_state.NH4]/(par[ind_N.K_NH4_Nit1]+conc[ind_state.NH4]) *
        #           conc[ind_state.O2]/(par[ind_N.K_O2_Nit1]+conc[ind_state.O2])
        # r_Nit1b = par[ind_N.k_Nit1] * f_N2O_Nit1 *
        #           conc[ind_state.NH4]^2/(par[ind_N.K_NH4_Nit1]+conc[ind_state.NH4])^2 *
        #           conc[ind_state.O2]/(par[ind_N.K_O2_Nit1]+conc[ind_state.O2])

        r_Nit1a = par[ind_N.k_Nit1] * (1.0-f_N2O_Nit1) *
                  Nsediment_Lim(conc[ind_state.NH4]/par[ind_N.K_NH4_Nit1];type=limtype) *
                  Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit1];type=limtype)
        r_Nit1b = par[ind_N.k_Nit1] * f_N2O_Nit1 *
                  Nsediment_Lim(conc[ind_state.NH4]/par[ind_N.K_NH4_Nit1];type=limtype)^2 *
                  Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit1];type=limtype)

        rates[ind_state.NH4] +=     - r_Nit1a - 2.0*r_Nit1b
        rates[ind_state.NO2] +=       r_Nit1a
        rates[ind_state.N2O] +=                     r_Nit1b
        rates[ind_state.O2]  += - 1.5*r_Nit1a - 2.0*r_Nit1b

    end

    function Nitrification_2!(rates,conc,par)

        # r_Nit2  = par[ind_N.f_Nit2_Nit1] * par[ind_N.k_Nit1] *
        #           conc[ind_state.NO2]/(par[ind_N.K_NO2_Nit2]+conc[ind_state.NO2]) *
        #           conc[ind_state.O2]/(par[ind_N.K_O2_Nit2]+conc[ind_state.O2])

        r_Nit2  = par[ind_N.f_Nit2_Nit1] * par[ind_N.k_Nit1] *
                  Nsediment_Lim(conc[ind_state.NO2]/par[ind_N.K_NO2_Nit2];type=limtype) *
                  Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit2];type=limtype)

        rates[ind_state.NO2] +=     - r_Nit2
        rates[ind_state.NO3] +=       r_Nit2
        rates[ind_state.O2]  += - 0.5*r_Nit2

    end

    function Nitrification_N1415_1!(rates,conc,par)

        f_N2O_Nit1 = par[ind_N.b_N2O_Nit1]*par[ind_N.a_N2O_Nit1]/(conc[ind_state.O2]+par[ind_N.a_N2O_Nit1])
        # Ji et al. 2018 https://doi.org/10.1029/2018GB005887  (equation modified to be bound to the interval [0,1])

        # r_Nit1a_part = par[ind_N.k_Nit1] * (1.0-f_N2O_Nit1) *
        #                1.0/(par[ind_N.K_NH4_Nit1]+conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15]) *
        #                conc[ind_state.O2]/(par[ind_N.K_O2_Nit1]+conc[ind_state.O2])
        # r_Nit1b_part = par[ind_N.k_Nit1] * f_N2O_Nit1 *
        #                1.0/(par[ind_N.K_NH4_Nit1]+conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])^2 *
        #                conc[ind_state.O2]/(par[ind_N.K_O2_Nit1]+conc[ind_state.O2])

        # r_Nit1a_N14_NO2   =       r_Nit1a_part * conc[ind_state.NH4_N14]
        # r_Nit1a_N15_NO2   =       r_Nit1a_part * conc[ind_state.NH4_N15] * (1.0-0.001*par[ind_N1415.eps_Nit1_NO2])
        # r_Nit1b_N1414_N2O =       r_Nit1b_part * conc[ind_state.NH4_N14] * conc[ind_state.NH4_N14] 
        # r_Nit1b_N1415_N2O = 2.0 * r_Nit1b_part * conc[ind_state.NH4_N14] * conc[ind_state.NH4_N15] * (1.0-0.001*par[ind_N1415.eps_Nit1_N2O])
        # r_Nit1b_N1515_N2O =       r_Nit1b_part * conc[ind_state.NH4_N15] * conc[ind_state.NH4_N15] * (1.0-0.001*par[ind_N1415.eps_Nit1_N2O])^2

        if (conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15] != 0.0) && (conc[ind_state.O2] != 0.0)

            r_Nit1a_part = par[ind_N.k_Nit1] * (1.0-f_N2O_Nit1) *
                           Nsediment_Lim((conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/par[ind_N.K_NH4_Nit1];type=limtype) *
                           Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit1];type=limtype)
            r_Nit1b_part = par[ind_N.k_Nit1] * f_N2O_Nit1 *
                           Nsediment_Lim((conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/par[ind_N.K_NH4_Nit1];type=limtype)^2 *
                           Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit1];type=limtype)

            r_Nit1a_N14_NO2   =       r_Nit1a_part * conc[ind_state.NH4_N14]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])
            r_Nit1a_N15_NO2   =       r_Nit1a_part * conc[ind_state.NH4_N15]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15]) * 
                                      (1.0-0.001*par[ind_N1415.eps_Nit1_NO2])
            r_Nit1b_N1414_N2O =       r_Nit1b_part * conc[ind_state.NH4_N14]*conc[ind_state.NH4_N14]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])^2 
            r_Nit1b_N1415_N2O = 2.0 * r_Nit1b_part * conc[ind_state.NH4_N14]*conc[ind_state.NH4_N15]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])^2 * 
                                      (1.0-0.001*par[ind_N1415.eps_Nit1_N2O])
            r_Nit1b_N1515_N2O =       r_Nit1b_part * conc[ind_state.NH4_N15]*conc[ind_state.NH4_N15]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])^2 * 
                                      (1.0-0.001*par[ind_N1415.eps_Nit1_N2O])^2

            rates[ind_state.NH4_N14]   += - r_Nit1a_N14_NO2 - 2.0*r_Nit1b_N1414_N2O -     r_Nit1b_N1415_N2O
            rates[ind_state.NH4_N15]   += - r_Nit1a_N15_NO2 -     r_Nit1b_N1415_N2O - 2.0*r_Nit1b_N1515_N2O
            rates[ind_state.NO2_N14]   +=   r_Nit1a_N14_NO2
            rates[ind_state.NO2_N15]   +=   r_Nit1a_N15_NO2
            rates[ind_state.N2O_N1414] +=   r_Nit1b_N1414_N2O
            rates[ind_state.N2O_N1415] +=   r_Nit1b_N1415_N2O
            rates[ind_state.N2O_N1515] +=   r_Nit1b_N1515_N2O
            rates[ind_state.O2]        +=  - 1.5*(r_Nit1a_N14_NO2+r_Nit1a_N15_NO2) - 2.0*(r_Nit1b_N1414_N2O+r_Nit1b_N1415_N2O+r_Nit1b_N1515_N2O)

        end

    end

    function Nitrification_N1415_2!(rates,conc,par)

        # r_Nit2_part  = par[ind_N.f_Nit2_Nit1] * par[ind_N.k_Nit1] *
        #                1.0/(par[ind_N.K_NO2_Nit2]+conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) *
        #                conc[ind_state.O2]/(par[ind_N.K_O2_Nit2]+conc[ind_state.O2])

        # r_Nit2_N14        =       r_Nit2_part  * conc[ind_state.NO2_N14]
        # r_Nit2_N15        =       r_Nit2_part  * conc[ind_state.NO2_N15] * (1.0-0.001*par[ind_N1415.eps_Nit2])

        if (conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15] != 0.0) && (conc[ind_state.O2] != 0.0)

            r_Nit2_part  = par[ind_N.f_Nit2_Nit1] * par[ind_N.k_Nit1] *
                           Nsediment_Lim((conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])/par[ind_N.K_NO2_Nit2];type=limtype) *
                           Nsediment_Lim(conc[ind_state.O2]/par[ind_N.K_O2_Nit2];type=limtype)

            r_Nit2_N14 = r_Nit2_part * conc[ind_state.NO2_N14]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])
            r_Nit2_N15 = r_Nit2_part * conc[ind_state.NO2_N15]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) * 
                         (1.0-0.001*par[ind_N1415.eps_Nit2])

            rates[ind_state.NO2_N14] += - r_Nit2_N14
            rates[ind_state.NO2_N15] += - r_Nit2_N15
            rates[ind_state.NO3_N14] +=   r_Nit2_N14
            rates[ind_state.NO3_N15] +=   r_Nit2_N15
            rates[ind_state.O2]      += - 0.5*(r_Nit2_N14+r_Nit2_N15)

        end

    end

    # Anaerobic ammonium oxidation (Anammox):
    
    # NH4(+) + NO2(-) → N2 + 2 H2O
    # NH4(+) + 1.3 NO2(-) + 0.15 CO2 → N2 + 0.3 NO3(-) + 0.15 CH2O + 1.85 H2O  (with side reaction)

    function Anammox!(rates,conc,par)

        if onestepden
            k_Anam = par[ind_N.f_Anam_Den] * par[ind_N.k_Den]
        else
            k_Anam = par[ind_N.f_Anam_Den2] * par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1]
        end

        # r_Anam = k_Anam *
        #          conc[ind_state.NH4]/(par[ind_N.K_NH4_Anam]+conc[ind_state.NH4]) *
        #          conc[ind_state.NO2]/(par[ind_N.K_NO2_Anam]+conc[ind_state.NO2]) *
        #          par[ind_N.K_O2_Anam]/(par[ind_N.K_O2_Anam]+conc[ind_state.O2]) 

        r_Anam = k_Anam *
                 Nsediment_Lim(conc[ind_state.NH4]/par[ind_N.K_NH4_Anam];type=limtype) *
                 Nsediment_Lim(conc[ind_state.NO2]/par[ind_N.K_NO2_Anam];type=limtype) *
                 Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Anam];type=limtype) 

        r_Anam_side = par[ind_N.f_Anam_side] * r_Anam

        rates[ind_state.NH4] += - r_Anam
        rates[ind_state.NO2] += - r_Anam - r_Anam_side
        rates[ind_state.NO3] +=            r_Anam_side
        rates[ind_state.N2]  +=   r_Anam

    end

    function Anammox_N1415!(rates,conc,par)

        if onestepden
            k_Anam = par[ind_N.f_Anam_Den] * par[ind_N.k_Den]
        else
            k_Anam = par[ind_N.f_Anam_Den2] * par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1]
        end

        # r_Anam_part = k_Anam *
        #               1.0/(par[ind_N.K_NH4_Anam]+conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15]) *
        #               1.0/(par[ind_N.K_NO2_Anam]+conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) *
        #               par[ind_N.K_O2_Anam]/(par[ind_N.K_O2_Anam]+conc[ind_state.O2]) 

        # r_Anam_N1414 = r_Anam_part * conc[ind_state.NH4_N14]*conc[ind_state.NO2_N14]
        # r_Anam_N1514 = r_Anam_part * conc[ind_state.NH4_N15]*conc[ind_state.NO2_N14] * (1-0.001*par[ind_N1415.eps_Anam_NH4])
        # r_Anam_N1415 = r_Anam_part * conc[ind_state.NH4_N14]*conc[ind_state.NO2_N15] * (1-0.001*par[ind_N1415.eps_Anam_NO2])
        # r_Anam_N1515 = r_Anam_part * conc[ind_state.NH4_N15]*conc[ind_state.NO2_N15] * (1-0.001*par[ind_N1415.eps_Anam_NO2]) * (1-0.001*par[ind_N1415.eps_Anam_NH4])

        if (conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15] != 0.0) && (conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15] != 0.0)

            r_Anam_part = k_Anam *
                          Nsediment_Lim((conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/par[ind_N.K_NH4_Anam];type=limtype) *
                          Nsediment_Lim((conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])/par[ind_N.K_NO2_Anam];type=limtype) *
                          Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_Anam];type=limtype) 

            r_Anam_N1414 = r_Anam_part * conc[ind_state.NH4_N14]*conc[ind_state.NO2_N14]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])
            r_Anam_N1514 = r_Anam_part * conc[ind_state.NH4_N15]*conc[ind_state.NO2_N14]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) * 
                           (1-0.001*par[ind_N1415.eps_Anam_NH4])
            r_Anam_N1415 = r_Anam_part * conc[ind_state.NH4_N14]*conc[ind_state.NO2_N15]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) * 
                           (1-0.001*par[ind_N1415.eps_Anam_NO2])
            r_Anam_N1515 = r_Anam_part * conc[ind_state.NH4_N15]*conc[ind_state.NO2_N15]/(conc[ind_state.NH4_N14]+conc[ind_state.NH4_N15])/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) * 
                           (1-0.001*par[ind_N1415.eps_Anam_NO2]) * (1-0.001*par[ind_N1415.eps_Anam_NH4])

            r_Anam_side_N1414 = par[ind_N.f_Anam_side] * r_Anam_N1414
            r_Anam_side_N1514 = par[ind_N.f_Anam_side] * r_Anam_N1514
            r_Anam_side_N1415 = par[ind_N.f_Anam_side] * r_Anam_N1415 * (1-0.001*par[ind_N1415.eps_Anam_side])
            r_Anam_side_N1515 = par[ind_N.f_Anam_side] * r_Anam_N1515 * (1-0.001*par[ind_N1415.eps_Anam_side])

            rates[ind_state.NH4_N14]  += - r_Anam_N1414 - r_Anam_N1415
            rates[ind_state.NH4_N15]  += - r_Anam_N1514 - r_Anam_N1515
            rates[ind_state.NO2_N14]  += - r_Anam_N1414 - r_Anam_N1514 - r_Anam_side_N1414 - r_Anam_side_N1514
            rates[ind_state.NO2_N15]  += - r_Anam_N1415 - r_Anam_N1515 - r_Anam_side_N1415 - r_Anam_side_N1515
            rates[ind_state.NO3_N14]  +=                                 r_Anam_side_N1414 + r_Anam_side_N1514
            rates[ind_state.NO3_N15]  +=                                 r_Anam_side_N1415 + r_Anam_side_N1515
            rates[ind_state.N2_N1414] +=   r_Anam_N1414
            rates[ind_state.N2_N1415] +=   r_Anam_N1514 + r_Anam_N1415
            rates[ind_state.N2_N1515] +=   r_Anam_N1515

        end

    end

    # Dissimilatory nitrate reduction to ammonimum (DNRA):

    # C106H263O110N16P + 212 NO3(-) → 106 HCO3(-) + 16 NH4(+) + HPO4(2−) + 212 NO2(−) + 92 H(+)

    # gamma_NH4_DNRA1 = 16/212 = 0.0755  [NH4(+) released from OM per NO3(-) consumed]

    # 3 C106H263O110N16P + 212 NO2(-) + 212 H2O + 148 H(+) → 318 HCO3(-) + 260 NH4(+) + 3 HPO4(2−) 

    # gamma_NH4_DNRA2 = (260-212)/212 = 48/212 = 0.226  [NH4(+) released from OM per NO2(-) consumed]

    function DNRA_1!(rates,conc,par)

        if onestepden
            k_DNRA1 = par[ind_N.f_DNRA1_Den] * par[ind_N.k_Den]
        else
            k_DNRA1 = par[ind_N.f_DNRA1_Den1] * par[ind_N.k_Den1]
        end

        # r_DNRA1 = k_DNRA1 *
        #           conc[ind_state.NO3]/(par[ind_N.K_NO3_DNRA1]+conc[ind_state.NO3]) *
        #           par[ind_N.K_O2_DNRA1]/(par[ind_N.K_O2_DNRA1]+conc[ind_state.O2])

        r_DNRA1 = k_DNRA1 *
                  Nsediment_Lim(conc[ind_state.NO3]/par[ind_N.K_NO3_DNRA1];type=limtype) *
                  Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_DNRA1];type=limtype)

        rates[ind_state.NO3] +=                            - r_DNRA1
        rates[ind_state.NO2] +=                              r_DNRA1
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_DNRA1] * r_DNRA1

    end

    function DNRA_2!(rates,conc,par)

        if onestepden
            k_DNRA2 = par[ind_N.f_DNRA2_Den] * par[ind_N.k_Den]
        else
            k_DNRA2 = par[ind_N.f_DNRA2_Den2] * par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1]
        end

        # r_DNRA2 = k_DNRA2 *
        #           conc[ind_state.NO2]/(par[ind_N.K_NO2_DNRA2]+conc[ind_state.NO2]) *
        #           par[ind_N.K_O2_DNRA2]/(par[ind_N.K_O2_DNRA2]+conc[ind_state.O2])

        r_DNRA2 = k_DNRA2 *
                  Nsediment_Lim(conc[ind_state.NO2]/par[ind_N.K_NO2_DNRA2];type=limtype) *
                  Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_DNRA2];type=limtype)

        rates[ind_state.NO2] +=                            - r_DNRA2
        rates[ind_state.NH4] +=                              r_DNRA2
        rates[ind_state.NH4] += par[ind_N.gamma_NH4_DNRA2] * r_DNRA2

    end

    function DNRA_N1415_1!(rates,conc,par)

        if onestepden
            k_DNRA1 = par[ind_N.f_DNRA1_Den] * par[ind_N.k_Den]
        else
            k_DNRA1 = par[ind_N.f_DNRA1_Den1] * par[ind_N.k_Den1]
        end

        # r_DNRA1_part = k_DNRA1 *
        #                1.0/(par[ind_N.K_NO3_DNRA1]+conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15]) *
        #                par[ind_N.K_O2_DNRA1]/(par[ind_N.K_O2_DNRA1]+conc[ind_state.O2])

        # r_DNRA1_N14 = r_DNRA1_part * conc[ind_state.NO3_N14]
        # r_DNRA1_N15 = r_DNRA1_part * (1.0-0.001*par[ind_N1415.eps_DNRA1])*conc[ind_state.NO3_N15]

        if conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15] != 0.0

            r_DNRA1_part = k_DNRA1 *
                           Nsediment_Lim((conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])/par[ind_N.K_NO3_DNRA1];type=limtype) *
                           Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_DNRA1];type=limtype)

            r_DNRA1_N14 = r_DNRA1_part * conc[ind_state.NO3_N14]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15])
            r_DNRA1_N15 = r_DNRA1_part * conc[ind_state.NO3_N15]/(conc[ind_state.NO3_N14]+conc[ind_state.NO3_N15]) *
                          (1.0-0.001*par[ind_N1415.eps_DNRA1])

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.NO3_N14] +=                                      - r_DNRA1_N14
            rates[ind_state.NO3_N15] +=                                      - r_DNRA1_N15
            rates[ind_state.NO2_N14] +=                                        r_DNRA1_N14
            rates[ind_state.NO2_N15] +=                                        r_DNRA1_N15
            rates[ind_state.NH4_N14] += par[ind_N.gamma_NH4_DNRA1] * R_OM[1] * (r_DNRA1_N14+r_DNRA1_N15)
            rates[ind_state.NH4_N15] += par[ind_N.gamma_NH4_DNRA1] * R_OM[2] * (r_DNRA1_N14+r_DNRA1_N15)

        end

    end

    function DNRA_N1415_2!(rates,conc,par)

        if onestepden
            k_DNRA2 = par[ind_N.f_DNRA2_Den] * par[ind_N.k_Den]
        else
            k_DNRA2 = par[ind_N.f_DNRA2_Den2] * par[ind_N.f_Den2_Den1] * par[ind_N.k_Den1]
        end

        # r_DNRA2_part = k_DNRA2 *
        #                1.0/(par[ind_N.K_NO2_DNRA2]+conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) *
        #                par[ind_N.K_O2_DNRA2]/(par[ind_N.K_O2_DNRA2]+conc[ind_state.O2])

        # r_DNRA2_N14 = r_DNRA2_part * conc[ind_state.NO2_N14]
        # r_DNRA2_N15 = r_DNRA2_part * (1.0-0.001*par[ind_N1415.eps_DNRA2])*conc[ind_state.NO2_N15]

        if conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15] != 0.0

            r_DNRA2_part = k_DNRA2 *
                           Nsediment_Lim((conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])/par[ind_N.K_NO2_DNRA2];type=limtype) *
                           Nsediment_Inh(conc[ind_state.O2]/par[ind_N.K_O2_DNRA2];type=limtype)

            r_DNRA2_N14 = r_DNRA2_part * conc[ind_state.NO2_N14]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15])
            r_DNRA2_N15 = r_DNRA2_part * conc[ind_state.NO2_N15]/(conc[ind_state.NO2_N14]+conc[ind_state.NO2_N15]) * 
                          (1.0-0.001*par[ind_N1415.eps_DNRA2])

            R_OM = calc_isotope_C1C2_fromCtot(1.0,par[ind_N1415.deltaN15_OM];R_std=R_N15N14_std)

            rates[ind_state.NO2_N14] +=                                      - r_DNRA2_N14
            rates[ind_state.NO2_N15] +=                                      - r_DNRA2_N15
            rates[ind_state.NH4_N14] +=                                        r_DNRA2_N14
            rates[ind_state.NH4_N15] +=                                        r_DNRA2_N15
            rates[ind_state.NH4_N14] += par[ind_N.gamma_NH4_DNRA2] * R_OM[1] * (r_DNRA2_N14+r_DNRA2_N15)
            rates[ind_state.NH4_N15] += par[ind_N.gamma_NH4_DNRA2] * R_OM[2] * (r_DNRA2_N14+r_DNRA2_N15)

        end

    end

    # Function to evalutate the right-hand side of the differential equations:
    # ------------------------------------------------------------------------

    function rhs!(dconc_dt,conc,p,t)

        # initialization and dimensionality check:

        if model == "N"
            n_conc  = length(statenames_required_N)
        else
            n_conc  = length(statenames_required_N1415)
        end
        n_state = length(conc)
        if n_state != n_conc * n_cells; error(string("*** Nsediment_run.rhs: inconsistent number of states (n_conc=",n_conc,", n_cells=",n_cells,", n_state=",n_state,")")); end

        # calculate diffusion rates:

        for j in 1:n_conc

            # uppermost cell:

            dconc_dt[j] = ( - diff_boundaries[j,1] * por_boundaries[1] * 
                                (conc[j]-conc_surf[j])  /(z_midpoints[1]-z_boundaries[1]) + 
                              diff_boundaries[j,2] * por_boundaries[2] * 
                                (conc[n_conc+j]-conc[j])/(z_midpoints[2]-z_midpoints[1]) ) / 
                          ( (z_boundaries[2]-z_boundaries[1]) * por_midpoints[1] )

            # inner cells:

            for i in 2:(n_cells-1)
                dconc_dt[(i-1)*n_conc+j] =  
                    ( - diff_boundaries[j,i] * por_boundaries[i] * 
                         (conc[(i-1)*n_conc+j]-conc[(i-2)*n_conc+j])/(z_midpoints[i]-z_midpoints[i-1]) +
                         diff_boundaries[j,i+1] * por_boundaries[i+1] * 
                          (conc[i*n_conc+j]-conc[(i-1)*n_conc+j])   /(z_midpoints[i+1]-z_midpoints[i]) ) / 
                    ( (z_boundaries[i+1]-z_boundaries[i]) * por_midpoints[i] )
            end

            # lowest cell:

            dconc_dt[(n_cells-1)*n_conc+j] =  
                  ( - diff_boundaries[j,n_cells] * por_boundaries[n_cells] * 
                        (conc[(n_cells-1)*n_conc+j]-conc[(n_cells-2)*n_conc+j])/(z_midpoints[n_cells]-z_midpoints[n_cells-1])
                    - flux_bottom[j] ) /
                  ( (z_boundaries[n_cells+1]-z_boundaries[n_cells]) * por_midpoints[n_cells] )
        end

        # add process rates:

        mod = model   # needed for Zygote compatibility!! (leads to segmentation fault otherwise)
        if mod == "N"

            for i in 1:n_cells

                MineralizationOx!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                  conc[((i-1)*n_conc+1):(i*n_conc)],p)
                if onestepden
                    Denitrification!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                     conc[((i-1)*n_conc+1):(i*n_conc)],p)
                else
                    Denitrification_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                       conc[((i-1)*n_conc+1):(i*n_conc)],p)
                    Denitrification_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                       conc[((i-1)*n_conc+1):(i*n_conc)],p)
                    Denitrification_3!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                       conc[((i-1)*n_conc+1):(i*n_conc)],p)
                end
                MineralizationSulfRed!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                    conc[((i-1)*n_conc+1):(i*n_conc)],p)
                MineralizationAnae!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                    conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Nitrification_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                 conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Nitrification_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                 conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Anammox!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                         conc[((i-1)*n_conc+1):(i*n_conc)],p)
                DNRA_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                        conc[((i-1)*n_conc+1):(i*n_conc)],p)
                DNRA_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                        conc[((i-1)*n_conc+1):(i*n_conc)],p)

            end

        elseif mod == "N1415"

            for i in 1:n_cells

                MineralizationOx_N1415!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                        conc[((i-1)*n_conc+1):(i*n_conc)],p)
                if onestepden
                    Denitrification_N1415!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                           conc[((i-1)*n_conc+1):(i*n_conc)],p)
                else
                    Denitrification_N1415_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                             conc[((i-1)*n_conc+1):(i*n_conc)],p)
                    Denitrification_N1415_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                             conc[((i-1)*n_conc+1):(i*n_conc)],p)
                    Denitrification_N1415_3!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                             conc[((i-1)*n_conc+1):(i*n_conc)],p)
                end
                MineralizationSulfRed_N1415!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                          conc[((i-1)*n_conc+1):(i*n_conc)],p)
                MineralizationAnae_N1415!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                          conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Nitrification_N1415_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                       conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Nitrification_N1415_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                                       conc[((i-1)*n_conc+1):(i*n_conc)],p)
                Anammox_N1415!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                               conc[((i-1)*n_conc+1):(i*n_conc)],p)
                DNRA_N1415_1!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                              conc[((i-1)*n_conc+1):(i*n_conc)],p)
                DNRA_N1415_2!(view(dconc_dt,((i-1)*n_conc+1):(i*n_conc)),
                              conc[((i-1)*n_conc+1):(i*n_conc)],p)
            end

        else

            error(string("*** Nsediment_run: unknown model ",model))

        end

    end

    # Set up and solve differential equations:
    # ----------------------------------------

    # extract model configuration parameters:

    parnames_required_config = ["depth","z1","por1","z2","por2","z3","por3"]
 
    for i in eachindex(parnames_required_config)
        if ! in(parnames_required_config[i],parnames)
            error(string("*** not all configuration parameters available: ",join(parnames_required_config,",")))
        end
    end

    depth = par[findfirst(isequal("depth"),parnames)]

    z1       = par[findfirst(isequal("z1"),parnames)]
    z2       = par[findfirst(isequal("z2"),parnames)]
    z3       = par[findfirst(isequal("z3"),parnames)]
    por1     = par[findfirst(isequal("por1"),parnames)]
    por2     = par[findfirst(isequal("por2"),parnames)]
    por3     = par[findfirst(isequal("por3"),parnames)]
    porosity = [z1 por1; z2 por2; z3 por3];

    # initialization:

    if model == "N"
        n_conc  = length(statenames_required_N)
    else
        n_conc  = length(statenames_required_N1415)
    end
    if n_cells < 3; error(string("*** Nsediment_run.rhs: n_cells=",n_cells," is too small; minimum value is 3")); end
 
    # numerical resolution of sediment across its depth:

    # cell boundaries:
    if f_cellsize < 1.1  # no expansion of cell size is factor is small
        z_boundaries = collect( ((1:(n_cells+1)).-1) ./ n_cells .* depth )   
    else                 # exponentially increasing size of cells across depth
        z_boundaries = collect( ( f_cellsize.^( ((1:(n_cells+1)).-1) ./ n_cells ) .- 1.0 ) ./ ( f_cellsize - 1.0 ) .* depth )
    end
    # cell midpoints:
    # z_midpoints = zeros(n_cells)
    z_midpoints = z_boundaries[1:n_cells]   # allocation compatible with Flowt64 and ForwardDiff.Dual (values will be overwritten)
    for i in 1:n_cells; z_midpoints[i] = 0.5 * (z_boundaries[i]+z_boundaries[i+1]); end
    # z values at which results will be made available (boundary points and cell midpoints):
    z_grid = vcat(z_boundaries[1],z_midpoints,z_boundaries[n_cells+1])

    # porosity interpolated at the z values at which results will be made available:

    if typeof(porosity) == Float64
        por_boundaries = repeat([porosity],n_cells+1)
        por_midpoints  = repeat([porosity],n_cells)
    else
        # por_fun = Interpolations.LinearInterpolation(porosity[:,1],porosity[:,2],     # problems with Zygote
        #                                              extrapolation_bc=Interpolations.Flat())
        por_fun = IntPolFun(porosity[:,1],porosity[:,2],extrapolation="constant")
        por_boundaries = por_fun.(z_boundaries)
        por_midpoints  = por_fun.(z_midpoints)
    end
    por_grid = vcat(por_boundaries[1],por_midpoints,por_boundaries[n_cells+1])

    # define indices for parameters and states:

    ind_N,errmsg = Nsediment_GetIndices(parnames,parnames_required_N)
    if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
    if model == "N"
        # state variables for N model without resolving isotopes:
        ind_state,errmsg = Nsediment_GetIndices(statenames_required_N,statenames_required_N)
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required state variable(s) not found: ",join(errmsg,", "))); end
        # diffusivities:
        ind_diff,errmsg = Nsediment_GetIndices(parnames,string.("D_",statenames_required_N))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        # surface concentrations:
        ind_conc,errmsg = Nsediment_GetIndices(parnames,string.("C_",statenames_required_N))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        # fluxes at lower boundary:
        ind_flux,errmsg = Nsediment_GetIndices(parnames,string.("F_",statenames_required_N))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
    elseif model == "N1415"
        # state variables for N isotope model:
        ind_state,errmsg = Nsediment_GetIndices(statenames_required_N1415,statenames_required_N1415)
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required state variable(s) not found: ",join(errmsg,", "))); end
        # diffusivities:
        ind_diff,errmsg = Nsediment_GetIndices(parnames,string.("D_",statenames_required_N1415))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        # surface concentrations:
        ind_conc,errmsg = Nsediment_GetIndices(parnames,string.("C_",statenames_required_N))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        ind_concdelta,errmsg = Nsediment_GetIndices(parnames,string.("deltaN15_C_",statenames_required_N[1:end-2]))  # omit O2 and SO4
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        # fluxes at lower boundary:
        ind_flux,errmsg = Nsediment_GetIndices(parnames,string.("F_",statenames_required_N))
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        ind_fluxdelta,errmsg = Nsediment_GetIndices(parnames,string.("deltaN15_F_",statenames_required_N[1:end-2]))  # omit O2 and SO4
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
        # additional parameters for isotope model:
        ind_N1415,errmsg = Nsediment_GetIndices(parnames,parnames_required_N1415)
        if length(errmsg) > 0; error(string("*** Nsediment_Run: Required parameter(s) not found: ",join(errmsg,", "))); end
    else
        error(string("*** Nsediment_run: unknown model ",model))
    end

    # calculate diffusivities:

    invtort = ( por_boundaries.^(par[ind_N.m_tort]-1.0) ) ./ par[ind_N.a_tort] 

    diff_boundaries = ( par[collect(ind_diff)] * transpose(invtort) ) + 
                      ( ones(n_conc) * transpose(par[ind_N.D_bio].*exp.(-z_boundaries./par[ind_N.depth_bio])) )

    # surface concentrations and bottom fluxes for model N:
    conc_surf   = view(par,collect(ind_conc))
    flux_bottom = view(par,collect(ind_flux))

    # modification to concentrations and bottom fluxes for model N1415:
    if model == "N1415"
        # collect surface concentrations for all compounds
        if occursin("N",statenames_required_N[1]) & ! occursin("N2",statenames_required_N[1])
            conc_surf_N1415 = calc_isotope_C1C2_fromCtot(conc_surf[1],par[ind_concdelta[1]];R_std=R_N15N14_std)
        elseif occursin("N2",statenames_required_N[1])
            conc_surf_N1415 = calc_isotope_C1C2C3_fromCtot(conc_surf[1],par[ind_concdelta[1]];R_std=R_N15N14_std)
        else
            conc_surf_N1415 = conc_surf[1]
        end
        for i in 2:length(statenames_required_N)
            if occursin("N",statenames_required_N[i]) & ! occursin("N2",statenames_required_N[i])
                conc_surf_N1415 = vcat(conc_surf_N1415,calc_isotope_C1C2_fromCtot(conc_surf[i],par[ind_concdelta[i]];R_std=R_N15N14_std))
            elseif occursin("N2",statenames_required_N[i])
                conc_surf_N1415 = vcat(conc_surf_N1415,calc_isotope_C1C2C3_fromCtot(conc_surf[i],par[ind_concdelta[i]];R_std=R_N15N14_std))
            else
                conc_surf_N1415 = vcat(conc_surf_N1415,conc_surf[i])
            end
        end
        conc_surf = conc_surf_N1415

        # collect bottom fluxes for N14 and N15 for all nitrogen compounds:
        if occursin("N",statenames_required_N[1]) & ! occursin("N2",statenames_required_N[1])
            flux_bottom_N1415 = calc_isotope_C1C2_fromCtot(flux_bottom[1],par[ind_fluxdelta[1]];R_std=R_N15N14_std)
        elseif occursin("N2",statenames_required_N[1])
            flux_bottom_N1415 = calc_isotope_C1C2C3_fromCtot(flux_bottom[1],par[ind_fluxdelta[1]];R_std=R_N15N14_std)
        else
            flux_bottom_N1415 = flux_bottom[1]
        end
        for i in 2:length(statenames_required_N)
            if occursin("N",statenames_required_N[i]) & ! occursin("N2",statenames_required_N[i])
                flux_bottom_N1415 = vcat(flux_bottom_N1415,calc_isotope_C1C2_fromCtot(flux_bottom[i],par[ind_fluxdelta[i]];R_std=R_N15N14_std))
            elseif occursin("N2",statenames_required_N[i])
                flux_bottom_N1415 = vcat(flux_bottom_N1415,calc_isotope_C1C2C3_fromCtot(flux_bottom[i],par[ind_fluxdelta[i]];R_std=R_N15N14_std))
            else
                flux_bottom_N1415 = vcat(flux_bottom_N1415,flux_bottom[i])
            end
        end
        flux_bottom = flux_bottom_N1415
    end

    # set up ODE problem:

    # conc_ini = zeros(n_conc*n_cells)
    conc_ini = repeat(conc_surf,n_cells)

    prob = OrdinaryDiffEq.ODEProblem(rhs!,
                                     conc_ini,
                                     (minimum(times),maximum(times)),
                                     par;
                                     saveat = times)

    # solve ODE problem:

    res = OrdinaryDiffEq.solve(prob,
                               IntAlg;
                               dt       = dt_ini,
                               # dtmax    = 0.05,
                               reltol   = reltol,
                               abstol   = abstol,
                               maxiters = maxiters)
    if res.retcode != :Success
        println("*** Nsediment_Run: OrdinaryDiffEq.solve failed")
        println(string("    Return code: ",string(res.retcode)))
        if eltype(par) == Float64
            println("    Parameters:")
            for i in eachindex(par)
                if ! (findfirst(isequal(parnames[i]),parnames) < i )
                    println(string("        ",parnames[i]," = ",par[i]))
                end
            end
        end
        flush(stdout)
        return missing, missing, missing, missing, missing, missing
    end

    # convert state vector and boundary conditions to vector of matrices:
    # (new allocation needed to be compatible with numbers and AD structures)

    states = reshape(vcat(conc_surf,                                                  # surface concentrations
                          res.u[1],                                                   # cell midpoint concentrations
                          res.u[1][((n_cells-1)*n_conc+1):(n_cells*n_conc)] .-        # lower boundary concentrations
                             (z_boundaries[n_cells+1]-z_midpoints[n_cells]) ./
                             (diff_boundaries[:,n_cells+1].*por_grid[n_cells+2]).*flux_bottom), # (consider flux from lower sediment)
                     n_conc,n_cells+2)
    states = vcat(states,transpose(por_grid))
    for i in 2:length(res.t)
        states  = cat(states,
                      vcat(reshape(vcat(conc_surf,                                                  # surface concentrations
                                        res.u[i],                                                   # cell midpoint concentrations
                                        res.u[i][((n_cells-1)*n_conc+1):(n_cells*n_conc)] .-        # lower boundary concentrations
                                           (z_boundaries[n_cells+1]-z_midpoints[n_cells]) ./
                                           (diff_boundaries[:,n_cells+1].*por_grid[n_cells+2]).*flux_bottom), # (consider flux from lower sediment)
                                   n_conc,n_cells+2),transpose(por_grid)),
                      dims=3)
    end
    if model == "N"
        statenames = [vcat(statenames_required_N,"porosity"),z_grid,res.t]
    else
        statenames = [vcat(statenames_required_N1415,"porosity"),z_grid,res.t]
    end

    # write states at end time point to file:

    if ! ismissing(filename)

        CSV.write(string(filename,"_states.txt"),
                  Tables.table(hcat(statenames[2],transpose(states[:,:,end]));
                               header=vcat(["Depth"],statenames[1])),
                  delim='\t');

    end

    # calculate fluxes and rates:

    if getrates

       # calculate fluxes:

        fluxes = deepcopy(states[1:(end-1),1:(end-1),:])  # allocate space with type compatibility with states for AD
        for j in 1:length(statenames[3])
            fluxes[:,1,j] = - ((states[1:n_conc,2,j]-states[1:n_conc,1,j])/(z_midpoints[1]-z_boundaries[1])) .*
                              diff_boundaries[:,1] .* por_boundaries[1]
            for i in 2:n_cells
                fluxes[:,i,j] = - ((states[1:n_conc,i+1,j]-states[1:n_conc,i,j])/(z_midpoints[i]-z_midpoints[i-1])) .*
                                  diff_boundaries[:,i] .* por_boundaries[i]
            end
            fluxes[:,n_cells+1,j] = - ((states[1:n_conc,n_cells+2,j]-states[1:n_conc,n_cells+1,j])/(z_boundaries[n_cells+1]-z_midpoints[n_cells])) .*
                                      diff_boundaries[:,n_cells+1] .* por_boundaries[n_cells+1]
        end
        fluxnames = [statenames[1][1:(end-1)],z_boundaries,statenames[3]]

        # calculate rates:

        function calc_rates(states,statenames,rates_fun!)
            n_conc  = length(statenames[1])-1   # subtract appended porosity
            n_cells = length(statenames[2])-2   # subtract boundary points and focus on cell midpoints
            n_t     = length(statenames[3])
            # rates = zeros(n_cells,n_conc,n_t)
            # rates = Array{Any,3}(undef,n_cells,n_conc,n_t)
            rates = deepcopy(states[1:(end-1),2:(end-1),:])  # allocate space with type compatibility with states for AD
            rates .= 0.0
            for j in 1:n_t
                for i in 1:n_cells
                    rates_fun!(view(rates,:,i,j),states[1:n_conc,1+i,j],par)
                end
            end
            return rates
        end

        if model == "N"

            rates = vcat([calc_rates(states,statenames,MineralizationOx!)],
                         if onestepden
                             [calc_rates(states,statenames,Denitrification!)]
                         else
                            [calc_rates(states,statenames,Denitrification_1!),
                             calc_rates(states,statenames,Denitrification_2!),
                             calc_rates(states,statenames,Denitrification_3!)]
                         end,
                         [calc_rates(states,statenames,MineralizationSulfRed!),
                          calc_rates(states,statenames,MineralizationAnae!),
                          calc_rates(states,statenames,Nitrification_1!),
                          calc_rates(states,statenames,Nitrification_2!),
                          calc_rates(states,statenames,Anammox!),
                          calc_rates(states,statenames,DNRA_1!),
                          calc_rates(states,statenames,DNRA_2!)])

        elseif model == "N1415"

            rates = vcat([calc_rates(states,statenames,MineralizationOx_N1415!)],
                         if onestepden
                             [calc_rates(states,statenames,Denitrification_N1415!)]
                         else
                             [calc_rates(states,statenames,Denitrification_N1415_1!),
                              calc_rates(states,statenames,Denitrification_N1415_2!),
                              calc_rates(states,statenames,Denitrification_N1415_3!)]
                         end,
                         [calc_rates(states,statenames,MineralizationSulfRed_N1415!),
                          calc_rates(states,statenames,MineralizationAnae_N1415!),
                          calc_rates(states,statenames,Nitrification_N1415_1!),
                          calc_rates(states,statenames,Nitrification_N1415_2!),
                          calc_rates(states,statenames,Anammox_N1415!),
                          calc_rates(states,statenames,DNRA_N1415_1!),
                          calc_rates(states,statenames,DNRA_N1415_2!)])

        end
        ratenames = [vcat(["MinOx"],
                          if onestepden
                              ["Den"]
                          else
                              ["Den1","Den2","Den3"]
                          end,
                          ["MinSulfRed","MinAnae","Nit1","Nit2","Anam","DNRA1","DNRA2"]),
                      statenames[1][1:(end-1)],
                      statenames[2][2:(end-1)],
                      statenames[3]]

        # write fluxes and rates at end time point to files:

        if ! ismissing(filename)

            CSV.write(string(filename,"_fluxes.txt"),
                      Tables.table(hcat(fluxnames[2],transpose(fluxes[:,:,end]));
                                   header=vcat(["Depth"],fluxnames[1])),
                      delim='\t');

            for i in 1:length(ratenames[1])
                CSV.write(string(filename,"_rates_",ratenames[1][i],".txt"),
                          Tables.table(hcat(ratenames[3],transpose(rates[i][:,:,end]));header=vcat(["Depth"],ratenames[2])),delim='\t');
            end

        end

        # return results:
        #    states indices: [subst,z,t], levels provided in statenames
        #    fluxes indices: [subst,z,t], levels provided in fluxnames
        #    rates indices:  [process,subst,z,t], levels provided in ratenames

        return states, statenames, fluxes, fluxnames, rates, ratenames

    else

        return states, statenames

    end

end


# Define prior:
# -------------

function Nsediment_LogPrior(par_est,parnames_est,par,parnames;
                            filename="")

    ind_sigma_eps            = findfirst(isequal("sigma_eps_pri"),parnames)
    ind_sigma_delta          = findfirst(isequal("sigma_deltaN15_OM_pri"),parnames)
    ind_sigmarel_K           = findfirst(isequal("sigmarel_K_pri"),parnames)
    ind_sigmarel_gamma       = findfirst(isequal("sigmarel_gamma_pri"),parnames)
    ind_sigmarel_f_Anam_side = findfirst(isequal("sigmarel_f_Anam_side_pri"),parnames)
    ind_sigmarel_f_Den       = findfirst(isequal("sigmarel_f_Den_pri"),parnames)
    ind_sigmarel_f_Nit       = findfirst(isequal("sigmarel_f_Nit_pri"),parnames)
    ind_sigmarel_f           = findfirst(isequal("sigmarel_f_pri"),parnames)   # f other than f_Anam_side, f_Den.., f_Nit..
    ind_sigmarel_depth_bio   = findfirst(isequal("sigmarel_depth_bio_pri"),parnames)
    ind_sigmarel_D_bio       = findfirst(isequal("sigmarel_D_bio_pri"),parnames)
    ind_sigmarel_sigma       = findfirst(isequal("sigmarel_sigma_pri"),parnames)
    ind_sigmarel_a_N2O_Nit1  = findfirst(isequal("sigmarel_a_N2O_Nit1_pri"),parnames)
    ind_sigmarel_b_N2O_Nit1  = findfirst(isequal("sigmarel_b_N2O_Nit1_pri"),parnames)

    logpri = 0.0
    if length(par_est) > 0
        if length(filename) > 0; priordef=Array{Any}(undef,length(par_est),4); end
        for i in eachindex(par_est)
            ind_mean = findfirst(isequal(string(parnames_est[i],"_pri")),parnames)
            if isnothing(ind_mean)
                logpri += 0.0
                if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Uniform",NaN,NaN]; end
            else
                if parnames_est[i][1:min(4,length(parnames_est[i]))] == "eps_"  # positive or negative values allowed for eps
                    if isnothing(ind_sigma_eps)
                        println("*** Nsediment_LogPrior: parameter sigma_eps_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Normal",par[ind_mean],NaN]; end
                    else
                        logpri += DistributionsAD.logpdf(DistributionsAD.Normal(par[ind_mean],par[ind_sigma_eps]),par_est[i])
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Normal",par[ind_mean],par[ind_sigma_eps]]; end
                    end
                elseif parnames_est[i] == "deltaN15_OM"  # positive or negative values allowed for delta
                    if isnothing(ind_sigma_delta)
                        println("*** Nsediment_LogPrior: parameter sigma_deltaN15_OM_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Normal",par[ind_mean],NaN]; end
                    else
                        logpri += DistributionsAD.logpdf(DistributionsAD.Normal(par[ind_mean],par[ind_sigma_delta]),par_est[i])
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Normal",par[ind_mean],par[ind_sigma_delta]]; end
                    end
                elseif parnames_est[i][1:min(2,length(parnames_est[i]))] == "K_"
                    if isnothing(ind_sigmarel_K)
                        println("*** Nsediment_LogPrior: parameter sigmarel_K_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_K]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_K]]; end
                elseif parnames_est[i][1:min(6,length(parnames_est[i]))] == "gamma_"
                    if isnothing(ind_sigmarel_gamma)
                        println("*** Nsediment_LogPrior: parameter sigmarel_gamma_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_gamma]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_gamma]]; end
                elseif parnames_est[i][1:min(12,length(parnames_est[i]))] == "f_Anam_side_"
                    if isnothing(ind_sigmarel_f_Anam_side)
                        println("*** Nsediment_LogPrior: parameter sigmarel_f_Anam_side_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_f_Anam_side]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_f_Anam_side]]; end
                elseif parnames_est[i][1:min(5,length(parnames_est[i]))] == "f_Den"
                    if isnothing(ind_sigmarel_f_Den)
                        println("*** Nsediment_LogPrior: parameter sigmarel_f_Den_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_f_Den]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_f_Den]]; end
                elseif parnames_est[i][1:min(5,length(parnames_est[i]))] == "f_Nit"
                    if isnothing(ind_sigmarel_f_Nit)
                        println("*** Nsediment_LogPrior: parameter sigmarel_f_Nit_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_f_Nit]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_f_Nit]]; end
                elseif parnames_est[i][1:min(2,length(parnames_est[i]))] == "f_"
                    if isnothing(ind_sigmarel_f)
                        println("*** Nsediment_LogPrior: parameter sigmarel_f_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_f]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_f]]; end
                elseif parnames_est[i] == "a_N2O_Nit1"
                    if isnothing(ind_sigmarel_a_N2O_Nit1)
                        println("*** Nsediment_LogPrior: parameter sigmarel_a_N2O_Nit1_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_a_N2O_Nit1]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_sigma]]; end
                elseif parnames_est[i] == "b_N2O_Nit1"
                    if isnothing(ind_sigmarel_b_N2O_Nit1)
                        println("*** Nsediment_LogPrior: parameter sigmarel_b_N2O_Nit1_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_b_N2O_Nit1]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_sigma]]; end
                elseif parnames_est[i] == "depth_bio"
                    if isnothing(ind_sigmarel_depth_bio)
                        println("*** Nsediment_LogPrior: parameter sigmarel_depth_bio_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_depth_bio]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_depth_bio]]; end
                elseif parnames_est[i] == "D_bio"
                    if isnothing(ind_sigmarel_D_bio)
                        println("*** Nsediment_LogPrior: parameter sigmarel_D_bio_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_D_bio]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_D_bio]]; end
                elseif parnames_est[i] == "sigma_C_a"
                    if isnothing(ind_sigmarel_sigma)
                        println("*** Nsediment_LogPrior: parameter sigmarel_sigma_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_sigma]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_sigma]]; end
                elseif parnames_est[i] == "sigma_C_b"
                    if isnothing(ind_sigmarel_sigma)
                        println("*** Nsediment_LogPrior: parameter sigmarel_sigma_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_sigma]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_sigma]]; end
                elseif parnames_est[i] == "sigma_delta"
                    if isnothing(ind_sigmarel_sigma)
                        println("*** Nsediment_LogPrior: parameter sigmarel_sigma_pri not found")
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** Nsediment_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_sigma]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_sigma]]; end
                else
                    if isnothing(ind_sigmarel_default)
                        println(string("*** Nsediment_LogPrior: parameter sigmarel_default_pri not found for parameter ",parnames_est[i]))
                        logpri = NaN
                        if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],NaN]; end
                    else
                        if (par[ind_mean] <= 0.0) | (par_est[i] <= 0.0)
                            println("*** NdynaNsediment_LogPriormics_LogPrior: parameter ",parnames_est[i],": prior mean (",par[ind_mean],")",
                                    " or current value (",par_est[i],") <= 0 (incompatible with lognormal distribution)")
                            logpri = NaN
                        else
                           sdlog   = sqrt(log(1+par[ind_sigmarel_default]^2))
                           meanlog = log(par[ind_mean]) - 0.5*sdlog^2
                           logpri += DistributionsAD.logpdf(DistributionsAD.LogNormal(meanlog,sdlog),par_est[i])
                        end
                    end
                    if length(filename) > 0; priordef[i,:] .= [parnames_est[i],"Lognormal",par[ind_mean],par[ind_mean]*par[ind_sigmarel_default]]; end
                end
            end
        end
        if length(filename) > 0
            CSV.write(string(filename,"_priordef.txt"),
                      Tables.table(priordef);header=["parameter","distribution","mean","sddev"],delim='\t');
        end
    end

    return logpri

end


# Model log likelihood function:
# ==============================

function Nsediment_Get_LogLikeli(model,times,par,parnames,obs,
                                 parnames_est;
                                 onestepden=false,limtype="monod",
                                 IntAlg=OrdinaryDiffEq.Tsit5(),
                                 n_cells=50,f_cellsize=1.0,dt_ini=0.1,reltol=1.0e-4,abstol=1.0e-4,maxiters=100000,
                                 verbose=false)

    function loglikeli(par_est)

        abserrormodel = false

        testoutput = false

        # if verbose & ( length(par_est) > 0 )
        #     println("")
        #     if :value in fieldnames(typeof(par_est[1]))
        #         println(string("  par = ",join(string(parnames_est[j],"=",par_est[j].value," ") for j in eachindex(par_est))))
        #     else
        #         println(string("  par = ",join(string(parnames_est[j],"=",par_est[j]," ") for j in eachindex(par_est))))
        #     end
        #     flush(stdout)
        # end

        state,statenames = Nsediment_Run(model,times,vcat(par_est,par),vcat(parnames_est,parnames);
                                         onestepden=onestepden,limtype=limtype,
                                         IntAlg=IntAlg,
                                         n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters)

        if ismissing(state)

            loglik = -Inf

        else

            # compounds:

            compounds = String[]
            for i in 1:(length(statenames[1])-1); compounds = vcat(compounds,split(statenames[1][i],"_")[1]); end  # -1: omit porosity that is appended to the states
            compounds_unique = unique(compounds)
            n_comp = length(compounds_unique)

            loglik = 0.0
            z = statenames[2]
    
            for compound in compounds_unique  # loop over compounds

                # search for observations of concentrations:

                for i in eachindex(obs)  # loop over observation data files
            
                    colnames = names(obs[i])
                    ind_Depth = findfirst(isequal("Depth"),colnames)
                    if ! isnothing(ind_Depth)

                        ind_conc = findfirst(isequal(compound),colnames)
                        if ! isnothing(ind_conc)

                            # concentration observations found (obs[i] column ind_conc); get simulation data:
                            ind = Int64[]
                            for j in 1:length(compounds); if compound==compounds[j]; push!(ind,j); end end
                            conc = sum(state[ind,:,end];dims=1)  # sum of all isotopes (array over depths)

                            # get standard deviation:

                            if abserrormodel
                                ind_sigma = findfirst(isequal(string("sigma_",compound)),vcat(parnames_est,parnames))
                                if isnothing(ind_sigma)
                                    error(string("loglikeli: parameter sigma_",compound," not found"))
                                end
                                sigma = vcat(par_est,par)[ind_sigma]
                            else
                                ind_sigma_a = findfirst(isequal("sigma_C_a"),vcat(parnames_est,parnames))
                                if isnothing(ind_sigma_a)
                                    error("loglikeli: parameter sigma_C_a not found")
                                end
                                sigma_a = vcat(par_est,par)[ind_sigma_a]
                                ind_sigma_b = findfirst(isequal("sigma_C_b"),vcat(parnames_est,parnames))
                                if isnothing(ind_sigma_b)
                                    error("loglikeli: parameter sigma_C_b not found")
                                end
                                sigma_b = vcat(par_est,par)[ind_sigma_b]
                            end

                            if testoutput & (eltype(par_est) == Float64)
                                println(compound)
                                println(hcat(z,transpose(conc)))
                                println(hcat(obs[i][:,ind_Depth],obs[i][:,ind_conc]))
                            end

                            # add contributions to log likelihood:
                            # modres_fun = Interpolations.LinearInterpolation(statenames[2],state[j,:,end],extrapolation_bc=Interpolations.Flat())
                            # modres_fun = Interpolations.interpolate(statenames[2],state[j,:,end],Interpolations.SteffenMonotonicInterpolation())
                            # modres_fun = BasicInterpolators.LinearInterpolator(statenames[2],state[j,:,end])
                            modres_fun = IntPolFun(z,conc)
                            loglik_part = 0.0
                            for k in 1:nrow(obs[i])
                                d = obs[i][k,ind_Depth]
                                if (d >= z[1]) & (d <= z[end]) & (!ismissing(obs[i][k,ind_conc]))
            
                                    # interpolate calculated concentration:
                                    conc_pred = modres_fun(d)
                                    if isnan(conc_pred)
                                        println("*** WARNING: loglikeli: concentration of ",compound," could not be calculated at d=",d,": data point omitted ***")
                                    else
                                        if abserrormodel
                                            loglik_part += DistributionsAD.logpdf(DistributionsAD.Normal(conc_pred,sigma),obs[i][k,ind_conc]) -
                                                           log(1.0-DistributionsAD.cdf(DistributionsAD.Normal(conc_pred,sigma),0.0))  # truncated at zero
                                        else
                                            # sigma = sigma_a*conc_pred+sigma_b
                                            sigma = sqrt(sigma_a*abs(conc_pred)+sigma_b^2)
                                            loglik_part += DistributionsAD.logpdf(DistributionsAD.Normal(conc_pred,sigma),obs[i][k,ind_conc]) -
                                                           log(1.0-DistributionsAD.cdf(DistributionsAD.Normal(conc_pred,sigma),0.0))  # truncated at zero
                                        end
                                    end
                                end
                            end
                            loglik += loglik_part
                            if verbose
                                if :value in fieldnames(typeof(loglik))
                                    println(string("       log likelihood(",compound,") = ",loglik_part.value))
                                else
                                    println(string("       log likelihood(",compound,") = ",loglik_part))
                                end
                            end

                            break

                        end

                    end

                end  # loop over observation data files

                # search for observations of deltaN15:

                for i in eachindex(obs)  # loop over observation data files
            
                    colnames = names(obs[i])
                    ind_Depth = findfirst(isequal("Depth"),colnames)
                    if ! isnothing(ind_Depth)

                        ind_delta = findfirst(isequal(string("deltaN15_",compound)),colnames)
                        if ! isnothing(ind_delta)

                            # deltaN15 observations found (obs[i] column ind_delta); 
                            # calculate deltaN15 from simulation results:

                            ind = Int64[]
                            for j in 1:length(compounds); if compound==compounds[j]; push!(ind,j); end end

                            if length(ind) == 2
                                N14 = findfirst(isequal(string(compound,"_N14")),statenames[1])
                                N15 = findfirst(isequal(string(compound,"_N15")),statenames[1])
                                if (! isnothing(N14) ) & (! isnothing(N15) )
                                    deltaN15 = calc_isotope_delta.(state[N14,:,end],state[N15,:,end];R_std=R_N15N14_std)
                                end
                            elseif length(ind) == 3
                                N1414 = findfirst(isequal(string(compound,"_N1414")),statenames[1])
                                N1415 = findfirst(isequal(string(compound,"_N1415")),statenames[1])
                                N1515 = findfirst(isequal(string(compound,"_N1515")),statenames[1])
                                if (! isnothing(N1414) ) & (! isnothing(N1415) ) & (! isnothing(N1515) )
                                    deltaN15 = calc_isotope_delta.(state[N1414,:,end],state[N1415,:,end],state[N1515,:,end];R_std=R_N15N14_std)
                                end
                            else
                                deltaN15 = Float64[]
                            end

                            if length(deltaN15) > 0

                                # get standard deviation:

                                if abserrormodel
                                    ind_sigma = findfirst(isequal(string("sigma_deltaN15_",compound)),vcat(parnames_est,parnames))
                                    if isnothing(ind_sigma)
                                        error(string("loglikeli: parameter sigma_",compound," not found"))
                                    end
                                else
                                    ind_sigma = findfirst(isequal("sigma_delta"),vcat(parnames_est,parnames))
                                    if isnothing(ind_sigma)
                                        error("loglikeli: parameter sigma_delta not found")
                                    end
                                end
                                sigma = vcat(par_est,par)[ind_sigma]
    
                                if testoutput & (eltype(par_est) == Float64)
                                    println(string("deltaN15_",compound))
                                    println(hcat(z,deltaN15))
                                    println(hcat(obs[i][:,ind_Depth],obs[i][:,ind_delta]))
                                end
    
                                # add contributions to log likelihood:
                                # modres_fun = Interpolations.LinearInterpolation(statenames[2],state[j,:,end],extrapolation_bc=Interpolations.Flat())
                                # modres_fun = Interpolations.interpolate(statenames[2],state[j,:,end],Interpolations.SteffenMonotonicInterpolation())
                                # modres_fun = BasicInterpolators.LinearInterpolator(statenames[2],state[j,:,end])
                                modres_fun = IntPolFun(z,deltaN15)
                                loglik_part = 0.0
                                for k in 1:nrow(obs[i])
                                    d = obs[i][k,ind_Depth]
                                    if (d >= z[1]) & (d <= z[end]) & (!ismissing(obs[i][k,ind_delta]))
                
                                        # interpolate calculated concentration:
                                        delta_pred = modres_fun(d)
                                        if isnan(delta_pred)
                                            println("*** WARNING: loglikeli: deltaN15 of ",compound," could not be calculated at d=",d,": data point omitted ***")
                                        else
                                            loglik_part += DistributionsAD.logpdf(DistributionsAD.Normal(delta_pred,sigma),obs[i][k,ind_delta])
                                        end
                                    end
                                end
                                loglik += loglik_part
                                if verbose
                                    if :value in fieldnames(typeof(loglik))
                                        println(string("       log likelihood(deltaN15_",compound,") = ",loglik_part.value))
                                    else
                                        println(string("       log likelihood(deltaN15_",compound,") = ",loglik_part))
                                    end
                                end
                                end
    
                            break

                        end

                    end

                end  # loop over observation data files

            end  # loop over compounds

        end

        if verbose
            if :value in fieldnames(typeof(loglik))
                println(string("       log likelihood(total) = ",loglik.value))
            else
                println(string("       log likelihood(total) = ",loglik))
            end
            flush(stdout)
        end

        return loglik

    end

    return loglikeli

end


# Define posterior:
# =================

function Nsediment_Get_LogPosterior(par_est,logprior,loglikeli,parnames_est;verbose=true)

    function logposterior(par_est)

        if verbose
            if length(par_est) > 0
                println("")
                if :value in fieldnames(typeof(par_est[1]))
                    println(string("  par = ",join(string(parnames_est[j],"=",par_est[j].value," ") for j in eachindex(par_est))))
                else
                    println(string("  par = ",join(string(parnames_est[j],"=",par_est[j]," ") for j in eachindex(par_est))))
                end
                flush(stdout)
            end
        end
    
        logpri = logprior(par_est)

        if verbose
            if :value in fieldnames(typeof(logpri))
                println(string("    log prior      = ",logpri.value))
            else
                println(string("    log prior      = ",logpri))
            end
            flush(stdout)
        end
    
        logpost = logpri
        if isfinite(logpri)
            loglik = loglikeli(par_est)
            logpost = logpri + loglik

            if verbose
                if :value in fieldnames(typeof(loglik))
                    println(string("    log likelihood = ",loglik.value))
                    println(string("    log posterior  = ",logpost.value))
                else
                    println(string("    log likelihood = ",loglik))
                    println(string("    log posterior  = ",logpost))
                end
                flush(stdout)
            end
        end
    
        GC.gc()   # explicit garbage collection needed to get rid of structures allocated in LogLikeli
    
        return logpost

    end
    
    return logposterior

end


# Plot profiles:
# ==============

function Nsediment_Plot(state,
                        statenames;
                        type      = "lastprofile",  # allprofiles
                        rates     = missing,
                        ratenames = missing,
                        obs       = missing,
                        min_conc  = missing,
                        max_conc  = missing,
                        min_delta = missing,
                        max_delta = missing,
                        plot_d15N = true,
                        filename  = "")

    colors = ["red","green","blue","cyan","magenta","springgreen","orange",
              "brown","yellow2","salmon","purple","black"]

    # check input:

    if ismissing(state) | ismissing(statenames)
        println("*** Nsediment_Plot: unable to plot as state input is missing")
        return
    end

    # compounds:

    compounds = String[]
    for i in 1:(length(statenames[1])-1); compounds = vcat(compounds,split(statenames[1][i],"_")[1]); end  # -1: omit porosity that is appended to the states
    compounds_unique = unique(compounds)
    n_comp = length(compounds_unique)

    # aggregate isotopes and calculate deltaN15:

    state_aggregated = copy(state[1:n_comp,:,:])   # allocate memory for aggregated states (content will be overwritten)
    statenames_aggregated = [compounds_unique,statenames[2],statenames[3]]
    deltaN15 = copy(state_aggregated)              # allocate memory for deltaN15s (content will be overwritten)
     
    for i in 1:n_comp

        compound = compounds_unique[i]
        ind = Int64[]
        for j in 1:length(compounds); if compound==compounds[j]; push!(ind,j); end end

        state_aggregated[i,:,:] = state[ind[1],:,:]
        deltaN15[i,:,:] .= NaN
        if length(ind) > 1
            for j in ind[2:end]
                state_aggregated[i,:,:] .+= state[j,:,:]
            end
            if length(ind) == 2
                N14 = findfirst(isequal(string(compound,"_N14")),statenames[1])
                N15 = findfirst(isequal(string(compound,"_N15")),statenames[1])
                if (! isnothing(N14) ) & (! isnothing(N15) )
                    deltaN15[i,:,:] .= calc_isotope_delta.(state[N14,:,:],state[N15,:,:];R_std=R_N15N14_std)
                end
            elseif length(ind) == 3
                N1414 = findfirst(isequal(string(compound,"_N1414")),statenames[1])
                N1415 = findfirst(isequal(string(compound,"_N1415")),statenames[1])
                N1515 = findfirst(isequal(string(compound,"_N1515")),statenames[1])
                if (! isnothing(N1414) ) & (! isnothing(N1415) ) & (! isnothing(N1515) )
                    deltaN15[i,:,:] .= calc_isotope_delta.(state[N1414,:,:],state[N1415,:,:],state[N1515,:,:];R_std=R_N15N14_std)
                end
            end
         end

    end

    if length(filename) > 0
        if ! all(isnan.(deltaN15[:,:,:]))
            if plot_d15N
                CSV.write(string(filename,"_deltaN15.txt"),
                          Tables.table(hcat(statenames_aggregated[2],transpose(deltaN15[:,:,end]));header=vcat(["Depth"],statenames_aggregated[1])),delim='\t');
            end
        end
    end

    # define title:

    title = string("Profiles (relaxation time=",statenames[3][end],"d)")
    if type == "allprofiles"
        if length(statenames[3]) <=5
            title = string("Profiles (times: ",join(statenames[3],","),"d)")
        else
            title = string("Profiles (times: ",join(string.(statenames[3][1:3]),","),",...,",statenames[3][end],"d)")
        end
    end

    # construct frame:

    if ismissing(min_conc); min_conc = min(0.0,minimum(state_aggregated[:,:,end])); end
    if ismissing(max_conc); max_conc = maximum(state_aggregated[:,:,end]); end
    p_conc  = Plots.plot(xlims=[minimum(statenames_aggregated[2]),maximum(statenames_aggregated[2])],
                         ylims=[min_conc,max_conc],
                         xlabel="z [cm]",ylabel="concentration [uM]",title=title)

    if ismissing(min_delta); min_delta = minimum(deltaN15[:,:,end]); end
    if ismissing(max_delta); max_delta = maximum(deltaN15[:,:,end]); end
    p_delta = Plots.plot(xlims=[minimum(statenames_aggregated[2]),maximum(statenames_aggregated[2])],
                         ylims=[min_delta,max_delta],
                         xlabel="z [cm]",ylabel="deltaN15",title=title)

    # plot dashed lines to show convergence (if asked to do so):

    if (type == "allprofiles") & (length(statenames_aggregated[3]) > 1)
        for i in 1:length(statenames_aggregated[1])
            for j in 1:length(statenames_aggregated[3])
                Plots.plot!(p_conc,statenames_aggregated[2],state_aggregated[i,:,j];
                            label="",linecolor=colors[i],linewidth=1,linestyle=:dash)
                if ! all(isnan.(deltaN15[i,:,j]))
                    Plots.plot!(p_delta,statenames_aggregated[2],deltaN15[i,:,j];
                                label="",linecolor=colors[i],linewidth=1,linestyle=:dash)
                end
            end
        end
    end

    # plot solid lines for final profile:

    for i in 1:length(statenames_aggregated[1])
        subst = statenames_aggregated[1][i]
        Plots.plot!(p_conc,statenames_aggregated[2],state_aggregated[i,:,end];   # plot last profile
                    label=subst,linecolor=colors[i],linewidth=1)
        if ! all(isnan.(deltaN15[i,:,end]))
            Plots.plot!(p_delta,statenames_aggregated[2],deltaN15[i,:,end];   # plot last profile
                        label=subst,linecolor=colors[i],linewidth=1)
        end

        # add observations:
        if ! ismissing(obs)
            for j in eachindex(obs)
                colnames = names(obs[j])
                ind_Depth = findfirst(isequal("Depth"),colnames)
                if ! isnothing(ind_Depth)
                    ind_Subst = findfirst(isequal(subst),colnames)
                    if ! isnothing(ind_Subst)
                        inds = findall(x->!ismissing(x),obs[j][:,ind_Subst])
                        Plots.plot!(p_conc,obs[j][inds,ind_Depth],Float64.(obs[j][inds,ind_Subst]);
                                    label="",linewidth=0,linecolor="white",
                                    markershape=:circle,markersize=3.0,markercolor=colors[i])
                    end
                    ind_delta = findfirst(isequal(string("deltaN15_",subst)),colnames)
                    if ! isnothing(ind_delta)
                        inds = findall(x->!ismissing(x),obs[j][:,ind_delta])
                        Plots.plot!(p_delta,obs[j][inds,ind_Depth],Float64.(obs[j][inds,ind_delta]);
                                    label="",linewidth=0,linecolor="white",
                                    markershape=:circle,markersize=3.0,markercolor=colors[i])
                    end
                end
            end
        end
    end

    if length(filename) > 0
        Plots.savefig(p_conc,string(filename,".pdf"))
        if ! all(isnan.(deltaN15[:,:,:]))
            if plot_d15N
                Plots.savefig(p_delta,string(filename,"_deltaN15.pdf"))
            end
        end
    end

    # plot rates

    if !ismissing(rates) && !ismissing(ratenames)

        n_procs = length(ratenames[1])

        # loop over compounds:

        for comp in compounds_unique

            # sum rates of different isotopes:

            r = zeros(length(ratenames[3]),n_procs)
            ind = findfirst(isequal(comp),ratenames[2])
            if isnothing(ind); ind = findall(isequal(string(comp,"_N")),SubString.(ratenames[2],1,min.(length.(ratenames[2]),length(comp)+2))); end
            for i in ind
                for j in 1:n_procs
                    r[:,j] .= r[:,j] .+ rates[j][i,:,end]   # add isotopes
                end
            end
  
            # construct frame:

            p_rates  = Plots.plot(xlims=[minimum(ratenames[3]),maximum(ratenames[3])],
                                  ylims=[minimum(r),maximum(r)],
                                  xlabel="z [cm]",ylabel=string("rates of ",comp," [uM/d]"),title=title)

            # plot rates of different processes:

            for i in 1:n_procs
                Plots.plot!(p_rates,ratenames[3],r[:,i];   # plot last profile
                            label=ratenames[1][i],linecolor=colors[i],linewidth=1)
            end

            if length(filename) > 0
                Plots.savefig(p_rates,string(filename,"_rates_",comp,".pdf"))
            end
        
        end

    end

end


# Auxiliary functions:
# ====================

function Nsediment_WritePar(parameters,filename;par_est=[],parnames_est=[])

    if length(par_est) > 0
        if length(parnames_est) != length(par_est); return; end
        parnames = convert(Array{String},parameters[:,"name"])
        for i in eachindex(parnames_est)
            ind = findfirst(x -> isequal(x,parnames_est[i]),parnames)
            parameters[ind,"value"] = par_est[i]
        end
    end

    CSV.write(filename,parameters,writeheader=true,delim='\t');

    return parameters

end


function Nsediment_WriteMargPriorDensities(par_est,parnames_est,par,parnames,filename)

    n_out = 5000

    if length(par_est) > 0
        for i in eachindex(par_est)
            ind = findfirst(isequal(string(parnames_est[i],"_pri")),parnames)
            if isnothing(ind)
                CSV.write(string(filename,"_margprior_",parnames_est[i],".txt"),
                          Tables.table(hcat(fill(0.0,n_out+1),fill(0.0,n_out+1)));writeheader=false,delim='\t');
            else
                if parnames_est[i][1:min(4,length(parnames_est[i]))] == "eps_"  # symmetric around prior mean
                    ind_sigma = findfirst(isequal(string("sigma_eps_pri")),parnames)
                    if isnothing(ind_sigma); sigma = 10.0; else sigma = par[ind_sigma]; end
                    p = par[ind].+collect(range(-3.0*sigma,3.0*sigma,n_out+1))
                elseif parnames_est[i][1:min(9,length(parnames_est[i]))] == "deltaN15_"  # symmetric around prior mean
                    ind_sigma = findfirst(isequal(string("sigma_deltaN15_pri")),parnames)
                    if isnothing(ind_sigma); sigma = 10.0; else sigma = par[ind_sigma]; end
                    p = par[ind].+collect(range(-3.0*sigma,3.0*sigma,n_out+1))
                elseif parnames_est[i][1:min(2,length(parnames_est[i]))] == "F_"  # symmetric around prior mean
                    ind_sigma = findfirst(isequal(string("sigma_F_pri")),parnames)
                    if isnothing(ind_sigma); sigma = 10.0; else sigma = par[ind_sigma]; end
                    p = par[ind].+collect(range(-3.0*sigma,3.0*sigma,n_out+1))
                else  # [ 0 , 5 * prior mean ]
                    p = 5.0 .* par[ind] .* collect(range(1.0/n_out,1.0,n_out))
                end
                d = zeros(length(p))
                for j in 1:length(p)
                    d[j] = exp(Nsediment_LogPrior([p[j]],[parnames_est[i]],par,parnames))
                end
                CSV.write(string(filename,"_margprior_",parnames_est[i],".txt"),
                          Tables.table(hcat(p,d));writeheader=false,delim='\t');
            end
        end
        Nsediment_LogPrior(par_est,parnames_est,par,parnames,filename=filename)
    end
end


function Nsediment_GetConcNames()

    statenames_required_N     = ["NH4","NO2","NO3","N2","N2O", "O2","SO4"]   # oxygen and sulfate need to be last
    # collect state names for isotope model (oder of compounds has to agree with the one above)
    statenames_required_N1415 = String[]
    for i in 1:length(statenames_required_N)
        if occursin("N",statenames_required_N[i]) & ! occursin("N2",statenames_required_N[i])
            statenames_required_N1415 = vcat(statenames_required_N1415,string.(statenames_required_N[i],["_N14","_N15"]))
        elseif occursin("N2",statenames_required_N[i])
            statenames_required_N1415 = vcat(statenames_required_N1415,string.(statenames_required_N[i],["_N1414","_N1415","_N1515"]))
        else
            statenames_required_N1415 = vcat(statenames_required_N1415,statenames_required_N[i])
        end
    end

    return statenames_required_N,statenames_required_N1415
end


function Nsediment_GetIndices(names,names_requested)

    ind    = Int[]
    errmsg = String[]
    for i in eachindex(names_requested)
        j = findfirst(isequal(names_requested[i]),names)
        if isnothing(j)
            errmsg = vcat(errmsg,string("name ",names_requested[i]," not found"))
            ind = vcat(ind,[0])
        else
            ind = vcat(ind,[j])
        end
    end

    return ( ind = (;zip(Symbol.(names_requested),ind)...) , errmsg = errmsg )

end


function Nsediment_Predict(model,times,parfile,parsampfile,filename_core;
                           onestepden=false,limtype="monod",
                           IntAlg=OrdinaryDiffEq.Tsit5(),
                           n_cells=50,f_cellsize=1.0,dt_ini=0.1,reltol=1.0e-4,abstol=1.0e-4,maxiters=100000,
                           thin=missing,verbose=true)

    n_out = 10

    # read parameters:
    # ----------------

    # all parameters:

    parameters = CSV.read(parfile,DataFrame;delim='\t',header=true)
    if ! ( ("name" in names(parameters)) & ("value" in names(parameters)) )
       error(string("*** The parameter input file ",parfile," does not contain colums name and value"))
    end
    parnames = convert(Array{String},parameters[:,"name"])
    par      = parameters[:,"value"]

    # sample of subset of inferred parameters:

    parsamp = CSV.read(parsampfile,DataFrame;delim='\t',header=true)
    parsampnames = names(parsamp)
    n_samples_MCMC = size(parsamp)[1]
    n_par_MCMC = size(parsamp)[2]

    # get indices of parameter subset within all parameters:

    ind_par = zeros(Int,n_par_MCMC)
    for i in 1:n_par_MCMC
        j = findfirst(isequal(parsampnames[i]),parnames)
        if isnothing(j); error(string("*** Nsediment_Predict: parameter ",parsampnames[i]," not found")); end
        ind_par[i] = j
    end

    # get thinning parameter:

    if ismissing(thin)
        thin = round(Int,par[findfirst(isequal("thin_predict"),parnames)])
        if isnothing(thin); thin = 1; end
    end

    # get indices of parameter sample for subsampling:

    ind_subsample = thin .* collect(0:floor(Int,(n_samples_MCMC-1)/thin)) .+ 1
    n_samples_predict = length(ind_subsample)

    # write parameter subsample (for postprocessing):

    CSV.write(string(filename_core,"_sample_parameters.txt"),parsamp[ind_subsample,:],delim='\t');

    # simulation for first sample point to allocate arrays:
    # -----------------------------------------------------

    if verbose
        println(string("starting uncertainty propagation to output ..."))
        flush(stdout)
    end

    # set parameter values and simulate:

    for i in 1:n_par_MCMC; par[ind_par[i]] = parsamp[ind_subsample[1],i]; end

    states_j,statenames_j,fluxes_j,fluxnames_j,rates_j,ratenames_j = 
                        Nsediment_Run(model,times,par,parnames;
                                      onestepden=onestepden,limtype=limtype,
                                      IntAlg=IntAlg,
                                      n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters);

    # allocate space and get concentrations:

    n_conc = length(statenames_j[1])-1
    conc = Array{Any,1}(undef,n_conc)
    concnames = statenames_j[1][1:(end-1)]
    for i in 1:n_conc
        conc[i] = Array{Float64,2}(undef,n_samples_predict,length(statenames_j[2]))
        conc[i][1,:] = states_j[i,:,end]
    end

    # allocate space and get fluxes:

    n_flux = length(fluxnames_j[1])
    fluxes = Array{Any,1}(undef,n_flux)
    fluxnames = fluxnames_j[1]
    for i in 1:n_flux
        fluxes[i] = Array{Float64,2}(undef,n_samples_predict,length(fluxnames_j[2]))
        fluxes[i][1,:] = fluxes_j[i,:,end]
    end

    # allocate space and get rates:

    n_process    = length(ratenames_j[1])
    n_rate       = length(ratenames_j[2])
    processnames = ratenames_j[1]
    ratenames    = ratenames_j[2]
    rates = Array{Any,1}(undef,n_process)
    for k in 1:n_process
        rates[k] = Array{Any,1}(undef,n_rate)
        for i in 1:n_rate
            rates[k][i] = Array{Float64,2}(undef,n_samples_predict,length(ratenames_j[3]))
            rates[k][i][1,:] = rates_j[k,][i,:,end]
        end
    end

    # simulations of other sample points:
    # -----------------------------------

    ind_output = floor.(Int,collect(1:n_out)/n_out*n_samples_predict)
    for j in 2:n_samples_predict

        # set parameter values and simulate:

        for i in 1:n_par_MCMC; par[ind_par[i]] = parsamp[ind_subsample[j],i]; end

        states_j,statenames_j,fluxes_j,fluxnames_j,rates_j,ratenames_j = 
                            Nsediment_Run(model,times,par,parnames;
                                          onestepden=onestepden,limtype=limtype,
                                          IntAlg=IntAlg,
                                          n_cells=n_cells,f_cellsize=f_cellsize,dt_ini=dt_ini,reltol=reltol,abstol=abstol,maxiters=maxiters);

        # get concentrations:

        for i in 1:n_conc; conc[i][j,:] = states_j[i,:,end]; end

        # get fluxes:

        for i in 1:n_flux; fluxes[i][j,:] = fluxes_j[i,:,end]; end

        # get rates:

        for k in 1:n_process
            for i in 1:n_rate
                rates[k][i][j,:] = rates_j[k][i,:,end]
            end
        end    

        # show progress:

        if verbose
            if j in ind_output
                println(string("simulation ",j," of ",n_samples_predict," completed"))
                flush(stdout)
            end
        end

        GC.gc()   # explicit garbage collection needed to get rid of structures allocated in simulation

    end

    # write result samples to files:
    # ------------------------------

    for i in 1:length(conc)
        CSV.write(string(filename_core,"_sample_conc_",concnames[i],".txt"),
                  Tables.table(conc[i];header=statenames_j[2]),delim='\t');
        CSV.write(string(filename_core,"_sample_fluxes_",fluxnames[i],".txt"),
                  Tables.table(fluxes[i];header=fluxnames_j[2]),delim='\t');
        for k in 1:n_process
            CSV.write(string(filename_core,"_sample_rates_",processnames[k],"_",ratenames[i],".txt"),
            Tables.table(rates[k][i];header=ratenames_j[3]),delim='\t');
        end
    end

end



