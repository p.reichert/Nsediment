# Nsediment

Julia scripts for diagenetic nitrogen isotopes modelling: Simulation of concentration profiles, posterior maximization, and Bayesian inference.

These scripts implement the model described in the following paper (link to be provided once published):
Alessandra Mazzoli, Peter Reichert, Claudia Frey, Cameron M. Callbeck, Tim J. Paulus, Jakob Zopfi, Moritz F. Lehmann
"A new comprehensive biogeochemical model for benthic N isotopes"
To be submitted in winter 2024/2025





## Summary of Steps Needed for Simulating New Data Sets

1. set-up observational data files (if available; see below for data format) and provide data set name in `Nsediment_run.jl`

3. provide model parameter file and set sediment surface boundary conditions (concentrations and d15N values) and bioturbation (maximum diffusivity and penetration depth); check priors and adapt if needed (the example priors should be quite universal)

4. do some runs to adapt (at least) the parameters `k_MinOx`, `k_Den1`, `k_MinSulfRed`, and `F_NH4` to facilitate parameter estimation; if needed adapt the transformation intervals to cover the expected posterior marginal distributions (see section `Define parameter transformation` in the file `Nsediment_run.jl`)

5. run some optimizations and check whether the results seem sufficiently good (simulation match the key pattern of the observed concentration and delta 15N profiles) for starting parameter inference

6. run Bayesian inference and postprocessing of inference results (check Markov chains for convergence and compare prior with posterior marginal distributions)

7. run prediction (uncertainty propagation to the results) and prediction postprocessing (check posterior simulated profiles with uncertainty bands)

See more details below about how to perform these steps.




## Data Format

### Parameter Input Data

Tab-separated text file at least with the columns `name` (parameter name), `value` (parameter value), and `fit` (either `yes` or `no`, indicating whether the parameter should be estimated if performing optimization or inference).

See example files `Lucerne_Apr2021_parameters.txt` and `Baldegg_May2021_parameters.txt` for the list of required parameters.


### Observational Data

Data can be provided as tab-separated text with a column `"Depth"` that contains the depth and additional columns out of

  `"NH4", "NO2", "NO3", "N2O", "N2", "O2"`
  
for concentrations of these compounds and for the model that distinguishes different isotopes columns out of

  `"deltaN15_NH4", "deltaN15_NO2", "deltaN15_NO3", "deltaN15_N2O", "deltaN15_N2_N15" `

for characterizing the ratio of 15N to 14N with deltaN15.

Multiple files are needed if different substances are measured at different depths levels. 
Labelling of a single data file is `<dataset>.txt`, labelling of multiple files is `<dataset>_i.txt` with `i = 1, 2, ...`, where `dataset` is a string variable defined in the code to select the correct data set.

Any available data is plotted with the model results and it is used in the likelihood function for parameter inference or optimization.



## Files

* **`Nsediment_run.jl`**: Script to execute runs, plotting, inference, uncertainty propagation (prediction), or reading and plotting previously stored results. Selection of tasks to be done in the section `choose which tasks to perform`.

* **`Nsediment.jl`**: All functions that do not depend on the specific application case. 
  Imported by `Nsediment_run.jl`.

* **`BayesianInference.jl`**: Interface to different MCMC samplers, imported by `Nsediment.jl`.

* **`Maximization.jl`**: Interface to different optimization algorithms, imported by `Nsediment.jl`.

* **`TransPar.jl`**: Framework for parameter transformations, imported by `BayesianInference.jl` and `Maximization.jl`.

* **`IntPol.jl`**: Interpolation compatible with Zygote, imported by `Nsediment.jl`.

* **`ProcessSimulationResults.R`**: R script to produce a table of depth-integrated rates of all compounds for all processes for a single simulation.

* **`ProcessInferenceResults.R`**: R script to produce plots of Markov chain, 1d prior and posterior marginals, and 2d marginal scatter plots of the results of Bayesian inference.

* **`ProcessPredictionResults.R`**: R script to produce a plot with simulation results for concentrations and delta 15N values, fluxes and rates including parameter uncertainty and total uncertainty confidence bands and data points (if available) and sample files for the depth-integrated rates of all compounds.




## Key Functions in `Nsediment.jl`

### Nsediment_run

Function to perform a simulation.

Mandatory arguments:
* `model`: either "N" for the model that does not resolve isotopes, or "N1415" for the model that does.
* `times`: `Float64` vector of time points to store profiles (multiple time points allow checking for convergence.
* `par`: `Float64` vector of model parameter values (see example files  `Lucerne_Apr2021_parameters.txt` and `Baldegg_May2021_parameters.txt` for the list of required parameters; note that a few parameters are read within `Nsediment_run` and passed as function arguments)
* `parnames`: `String` vector of names of parameters `par` (see file `Nsediment_run.jl` for a list of parameters)

Optional arguments (for defaults see source code):
* `IntAlg`: integration algorithm (see https://diffeq.sciml.ai/stable/solvers/ode_solve/)
* `n_cells`: number of cells to numerically resolve the sediment layer; see argument `f_cellsize` for a description of cell size variation
* `f_cellsize`: approximate factor of the size of the deepest cell to the cell closest to the surface; the default value of 1.0 leads to cells of equal size; cell boundaries are calculated according to b_i = (i-1)/n_cells*depth for f_cellsize < 1.1 (equal spacing) and b_i = (f_cellsize^((i-1)/n_cells)-1) / ( f_cellsize-1 ) * depth for f_cellsize >= 1.1 (exponentially growing cell sizes) (i=1,..n_cells+1); results are made available at the top and bottom boundaries and at the cell midpoints [f_cellsize is the ratio of the derivatives db_i/di at the full depth and at the surface]
* `dt`: initial time step [d]
* `reltol`: relative accuracy of integration algorithm
* `abstol`: absolute accuracy of integration algorithm
* `maxiters`: maximum number of iterations of the integration algorithm
* `filename`: core name of file for numeric output of simulation results; will be appended by `_states.txt` for states and by `_rates_MinOx.txt`, `_rates_Den1.txt`, `_rates_Den2.txt`, `_rates_Den3.txt`, `_rates_Nit1.txt`, `_rates_Nit2.txt`, `_rates_MinAnae.txt`, `_rates_DNRA1.txt` and `_rates_DNRA2.txt`  for transformation rates of steps of the processes oxic mineralization, denitrification, nitrification, anammox and DNRA, respectively
* `getrates`: logical variable to indicate whether fluxes and rates should be returned (for performance reasons set to false during optimization and inference)

Return values:
* `state`: 3d `Float64` array of concentrations and porosity [substance (and porosity),depth,time]
* `statenames`: vector of arrays of dimension names (for substances and porosity) or numerical values (of depth and time) [substance,depth,time]

If `getrates` is set to true, additional outputs
* `fluxes`: 3d `Float64` array of fluxes [substance,depth,time]
* `fluxnames`: vector of arrays of dimension names (for fluxes) or numerical values (of depth and time) [substance,depth,time]
* `rates`: 1d vector of 3d `Float64` arrays of rates for the different processes; for each process the `rates` element consists of the dimensions [substance,depth,time]
* `ratenames`: vector of arrays of dimension names (for processes and rates) or numerical values (of depth and time) [process,substance,depth,time]

Output:
* it the argument `filename` is provided: text files of numerical simulation rates with this filename appended by `_states.txt` for states, by `_fluxes.txt` for fluxes, and by `_rates_MinOx.txt`, `_rates_Den1.txt`, `_rates_Den2.txt`, `_rates_Den3.txt`, `_rates_Nit1.txt`, `_rates_Nit2.txt`, `_rates_MinAnae.txt`, `_rates_DNRA1.txt` and `_rates_DNRA2.txt` for transformation rates of steps of the processes oxic mineralization, denitrification, nitrification, anammox and DNRA, respectively
* `getrates`: logical variable to indicate whether fluxes and rates should be returned (for performance reasons set to false during optimization and inference)



### Nsediment_Plot

Function to plot simulation results.

Mandatory arguments:
* `state`: calculated states from function `Nsediment_run`
* `statenames`: annotation of states `state` from `function `N_sediment_run

Optional arguments (for defaults see source code):
* `type`: either `lastprofile` to get a plot of the profiles at the final time point, or `allprofiles` to get additional dashed lines for profiles at the other time points
* `obs`: vector of data frames with observed substance profiles. Each date frame needs a column the name of which starts with `Depth`; columns with names starting with the names of state variables (see `statenames[1]` will be interpreted as observed profiles of this state variable and will be plotted in addition to the simulation results
* `filename`: core name of output file to save the plot

Output:
* pdf output files of the profiles of nitrogen compounds (sums of the different isotopes) and delta 15N values at the last simulation time point (the core filename is appended by `_simulation.pdf` for concentration profiles, by `_simulation_noN2concscaling.pdf` for concentration profiles without considering scaling for N2 concentrations, by `_simulation_deltaN15.pdf` for profiles of delta 15N; additional output with filenmanes appended by `_simconv.pdf`and `_simconv_deltaN15.pdf` contain plots for all simulation output time points to check convergence to steady-state)



### Nsediment_Predict

Function to propagate a parameter sample to an sample of simulation results.

Mandatory arguments:
* `model`: either "N" for the model that does not resolve isotopes, "N1415" for the model that does.
* `times`: `Float64` vector of time points to store profiles (multiple time points allow checking for convergence.
* `parfile`: file name for parameter file (see example files  `Lucerne_Apr2021_parameters.txt` and `Baldegg_May2021_parameters.txt`)
* `parsampfile`: file name for parameter sample file (typically the Markov chain output from inference; for consistent predicitions it is important that this sample file corresponds to the parameter input our output file used for or written by the inference process).
* `filename_core`: core file name for output files.

Optional arguments (for defaults see source code):
* `IntAlg`: integration algorithm (see https://diffeq.sciml.ai/stable/solvers/ode_solve/)
* `n_cells`: number of cells to numerically resolve the sediment layer; see argument `f_cellsize` for a description of cell size variation
* `f_cellsize`: approximate factor of the size of the deepest cell to the cell closest to the surface; the default value of 1.0 leads to cells of equal size; cell boundaries are calculated according to b_i = (i-1)/n_cells*depth for f_cellsize < 1.1 (equal spacing) and b_i = (f_cellsize^((i-1)/n_cells)-1) / ( f_cellsize-1 ) * depth for f_cellsize >= 1.1 (exponentially growing cell sizes) (i=1,..n_cells+1); results are made available at the top and bottom boundaries and at the cell midpoints [f_cellsize is the ratio of the derivatives db_i/di at the full depth and at the surface]
* `dt`: initial time step [d]
* `reltol`: relative accuracy of integration algorithm
* `abstol`: absolute accuracy of integration algorithm
* `maxiters`: maximum number of iterations of the integration algorithm
* `thin`: thinning to be applied for reducing the sample size of the results outputs compared to the input parameter sample (if provided, this value overwrites the value provided in the parameter file)

Output:
* text files of samples of modelled profiles with depth in the first row and profile samples in the following rows: filename appended by `_sample_conc_<X>.txt` for concentration profiles, filename appended by `_sample_fluxes_<X>.txt` for flux profiles, and filename appended by `_sample_rates_<Y>_<X>.txt` for rates profiles (`<X>` are the modelled compounds and `<Y>` are the different process steps)



## Some Auxiliary Functions in `Nsediment.jl`

### calc_isotope_delta (version for compounds with one isotope atom)

Function to calculate isotope delta = ( C2/C1 / R_std - 1 ) * 1000 from individual isotope concentrations C1 and C2

Mandatory arguments:
*  `C1`: concentration of the compound with one atom of the more abundant isotope
*  `C2`: concentration of the compound with one atom of the less abundant isotope

Optional arguments:

*	`R_std`: standard isotope ratio (default value of 0.0036765 is valid for N15/N14)

Return value:

*	`delta': ( C2/C1 / R_std - 1 ) * 1000


### calc_isotope_delta (version for compounds with two isotope atoms)

Function to calculate isotope delta = ( (C2+2.0*C3)/(2.0*C1+C2) / R_std - 1 ) * 1000 from individual isotope concentrations C1 and C2

Mandatory arguments:
*  `C1`: concentration of the compound with two atoms of the more abundant isotope
*  `C2`: concentration of the compound with one atom each of the more abundant and one of the less abundant isotopes
*  `C3`: concentration of the compound with two atoms of the less abundant isotope

Optional arguments:

*	`R_std`: standard isotope ratio (default value of 0.0036765 is valid for N15/N14)

Return value:

*	`delta': ( (C2+2.0*C3)/(2.0*C1+C2) / R_std - 1 ) * 1000


### calc_isotope_C1C2_fromCtot (version for compounds with one isotope atom)

Function to calculate the isotope concentrations from the total concentration Ctot, and from delta = ( C2/C1 / R_std - 1 ) * 1000

Mandatory arguments:
*  `Ctot`: total concentration of both isotopes Ctot = C1 + C2
*  `delta`: delta = ( C2/C1 / R_std - 1 ) * 1000

Optional arguments:

*	`R_std`: standard isotope ratio (default value of 0.0036765 is valid for N15/N14)

Return value:

*	`[C1,C2]': array of the concentrations of both isotopes


### calc_isotope_C1C2C3_fromCtot (version for compounds with two isotope atoms)

Function to calculate the isotope concentrations from the total concentration Ctot, and from delta = ( (C2+2.0*C3)/(2.0*C1+C2) / R_std - 1 ) * 1000

Note: the problem is not fully specified with two inputs and three outputs.
To make the solution unique, we assume that the ratio of C1 : C2 : C3 = 1 : 2r : r^2, where r is the ratio of the rare to the abundant isotope.
See also https://doi.org/10.4141/cjss87-075 ( equation 4 with a=1/(1+r) and b=r/(1+r) )

Mandatory arguments:
*  `Ctot`: total concentration of all compounds Ctot = C1 + C2 + C3
*  `delta`: delta = ( (C2+2.0*C3)/(2.0*C1+C2) / R_std - 1 ) * 1000

Optional arguments:

*	`R_std`: standard isotope ratio (default value of 0.0036765 is valid for N15/N14)

Return value:

*	`[C1,C2,C3]': array of the concentrations of all three compounds (with two of the abundant isotopes, with one abundant and one less abundant isotopes, and with two less abundant isotopes)


### Nsediment_Get_LogLikeli

Function to Get the log likelihood function of the model as a function of selected parameters only.

Mandatory arguments:
* `model`: either "N" for the model that does not resolve isotopes, "N1415" for the model that does.
* `times`: `Float64` vector of time points to store profiles (multiple time points allow checking for convergence.
* `par`: `Float64` vector of model parameter values (see file `Nsediment_run.jl` for a list of parameters)
* `parnames`: `String` vector of names of parameters `par` (see file `Nsediment_run.jl` for a list of parameters)
* `obs`: vector of data frames with observed substance profiles. Each date frame needs a column `Depth`; columns with names of state variables (see `concnames`) will be interpreted as observed profiles of this state variable
* `parnames_est`: `String` vector of names of selected parameters that will be arguments to the returned likelihood function

Optional arguments (for defaults see source code):
* `IntAlg`: integration algorithm (see https://diffeq.sciml.ai/stable/solvers/ode_solve/)
* `n_cells`: number of cells to numerically resolve the sediment layer; see argument `f_cellsize` for a description of cell size variation
* `f_cellsize`: approximate factor of the size of the deepest cell to the cell closest to the surface; the default value of 1.0 leads to cells of equal size; cell boundaries are calculated according to b_i = (i-1)/n_cells*depth for f_cellsize < 1.1 (equal spacing) and b_i = (f_cellsize^((i-1)/n_cells)-1) / ( f_cellsize-1 ) * depth for f_cellsize >= 1.1 (exponentially growing cell sizes) (i=1,..n_cells+1); results are made available at the top and bottom boundaries and at the cell midpoints [f_cellsize is the ratio of the derivatives db_i/di at the full depth and at the surface]
* `dt`: initial time step [d]
* `reltol`: relative accuracy of integration algorithm
* `abstol`: absolute accuracy of integration algorithm
* `maxiters`: maximum number of iterations of the integration algorithm

Return value:
* log likelihood function with a single argument, the vector of parameter values at which the log likelihood should be evaluated; the parameter values have to correspond to the parameter names specified as `parnames_est`


