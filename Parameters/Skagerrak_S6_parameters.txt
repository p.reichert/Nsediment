name	value	unit	meaning	category	fit	comment	reference
k_MinOx	329.3626521370365	uMol/L/d		N	yes	estimated	
k_Den1	191.18382653302942	uMol/L/d		N	yes	estimated	
f_Den2_Den1	3.8645214196125584	uMol/L/d		N	yes	estimated	
f_Den3_Den1	3.683946854952363	uMol/L/d		N	yes	estimated	
k_MinSulfRed	0.0	uMol/L/d		N	no	estimated	
k_MinAnae	0.7876391226448047	uMol/L/d		N	yes	estimated	
k_Nit1	18.470343640882703	uMol/L/d		N	yes	estimated	
k_Nit2	8.583002527056465	uMol/L/d		N	yes	estimated	
f_Anam_Den2	0.010026529146659862	-		N	yes	estimated	
f_DNRA1_Den1	0.00973922421162074	-		N	yes	estimated	
f_DNRA2_Den2	0.008732777634700341	-		N	yes	estimated	
f_Anam_side	0.312646157299959	-		N	yes	estimated	
K_O2_MinOx	8.627141840599512	uMol/L		N	yes	estimated	
K_O2_Den1	2.9062704360710514	uMol/L		N	yes	estimated	
K_O2_Den2	2.8494725360783604	uMol/L		N	yes	estimated	
K_O2_Den3	0.09720100692657055	uMol/L		N	yes	estimated	
K_NO3_Den1	3.0675846924775727	uMol/L		N	yes	estimated	
K_NO2_Den2	0.3775283457580843	uMol/L		N	yes	estimated	
K_N2O_Den3	3.4177486285448033	uMol/L		N	yes	estimated	
K_O2_MinSulfRed	4.141976442	uMol/L		N	no	estimated	
K_NO3_MinSulfRed	3.910834504	uMol/L		N	no	estimated	
K_SO4_MinSulfRed	19.70648081	uMol/L		N	no	estimated	
K_O2_MinAnae	4.727713088363056	uMol/L		N	yes	estimated	
K_NO3_MinAnae	4.878007610965684	uMol/L		N	yes	estimated	
K_O2_Nit1	3.8313322810646206	uMol/L		N	yes	estimated	
K_O2_Nit2	0.7785518126137322	uMol/L		N	yes	estimated	
K_NH4_Nit1	2.1265548765552262	uMol/L		N	yes	estimated	
K_NO2_Nit2	0.9210440557242574	uMol/L		N	yes	estimated	
K_NH4_Anam	0.8947456460981839	uMol/L		N	yes	estimated	
K_NO2_Anam	5.209142075764335	uMol/L		N	yes	estimated	
K_O2_Anam	2.4544465095761923	uMol/L		N	yes	estimated	
K_O2_DNRA1	2.94077180504296	uMol/L		N	yes	estimated	
K_O2_DNRA2	3.085006164706382	uMol/L		N	yes	estimated	
K_NO3_DNRA1	2.332699732435356	uMol/L		N	yes	estimated	
K_NO2_DNRA2	0.3752500671593732	uMol/L		N	yes	estimated	
a_N2O_Nit1	0.20053047505830884	uMol/L		N	yes	estimated	
b_N2O_Nit1	0.0761579494585355	-		N	yes	estimated	
gamma_NH4_MinOx	0.12569294261790537	-		N	yes	estimated	
gamma_NH4_Den1	0.3875010904431925	-		N	yes	estimated	
gamma_NH4_Den2	0.3909241301389132	-		N	yes	estimated	
gamma_NH4_Den3	0.2013024160745677	-		N	yes	estimated	
gamma_NH4_MinSulfRed	0.304689334	-		N	no	estimated	
gamma_NH4_DNRA1	0.25131691457863364	-		N	yes	estimated	
gamma_NH4_DNRA2	0.7484205692660089	-		N	yes	estimated	
deltaN15_OM	4.898439478152562	permille		N1415	yes	estimated	
eps_Den1	9.707277531371085	permille		N1415	yes	estimated	
eps_Den2	13.804317954838114	permille		N1415	yes	estimated	
eps_Den3	7.409111736300936	permille		N1415	yes	estimated	
eps_Nit1_NO2	23.847609786227288	permille		N1415	yes	estimated	
eps_Nit1_N2O	37.649447429093364	permille		N1415	yes	estimated	
eps_Nit2	-11.259059430826145	permille		N1415	yes	estimated	
eps_Anam_NH4	19.364377250495643	permille		N1415	yes	estimated	
eps_Anam_NO2	17.265702231221677	permille		N1415	yes	estimated	
eps_Anam_side	-31.220040077478444	permille		N1415	yes	estimated	
eps_DNRA1	18.00744735466646	permille		N1415	yes	estimated	
eps_DNRA2	14.305115470703502	permille		N1415	yes	estimated	
F_NH4	-20.904463718667976	nMol/cm^2/d		N	yes	estimated	
deltaN15_F_NH4	3.2181931299489523	permille		N1415	yes	estimated	
sigma_C_a	2.3301323897505566	uMol/L			yes	estimated	
sigma_C_b	1.5317316405889392	uMol/L			yes	estimated	
sigma_delta	0.6395462830148168	permille			yes	estimated	
F_NO2	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
F_NO3	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
F_N2O	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
F_N2	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
F_O2	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
F_SO4	0.0	nMol/cm^2/d		N	no	lower boundary condition	no flux from deeper sediment layers
deltaN15_F_NO2	0.0	permille		N1415	no	lower boundary condition	not relevant as F_NO2 is zero
deltaN15_F_NO3	0.0	permille		N1415	no	lower boundary condition	not relevant as F_NO3 is zero
deltaN15_F_N2O	0.0	permille		N1415	no	lower boundary condition	not relevant as F_N2O is zero
deltaN15_F_N2	0.0	permille		N1415	no	lower boundary condition	not relevant as F_N2 is zero
C_NH4	0.0	uMol/L		N	no	upper boundary condition	data
C_NO2	0.0	uMol/L		N	no	upper boundary condition	data
C_NO3	11.955	uMol/L		N	no	upper boundary condition	data
C_N2	717.0	uMol/L		N	no	upper boundary condition	input by Moritz Lehmann (8th May 2024)
C_N2O	0.015	uMol/L		N	no	upper boundary condition	input by Moritz Lehmann and Claudia Frey (8th May 2024)
C_O2	250.0	uMol/L		N	no	upper boundary condition	data
C_SO4	30000.0	uMol/L		N	no	upper boundary condition	data
deltaN15_C_NH4	0.0	permille		N1415	no	upper boundary condition	data from 0.5 cm
deltaN15_C_NO2	0.0	permille		N1415	no	upper boundary condition	not relevant as C_NO2 is zero
deltaN15_C_NO3	4.751	permille		N1415	no	upper boundary condition	data
deltaN15_C_N2	0.0	permille		N1415	no	upper boundary condition	input by Moritz Lehmann (8th May 2024)
deltaN15_C_N2O	6.2	permille		N1415	no	upper boundary condition	input by Claudia Frey (8th May 2024)
D_NH4	1.02	cm^2/d		N	no		Furrer 1996
D_NO2	1.02	cm^2/d		N	no		Furrer 1997
D_NO3	1.02	cm^2/d		N	no		Furrer 1998
D_N2	0.959	cm^2/d		N	no		Broecker 1974
D_N2O	1.166	cm^2/d		N	no		Broecker 1975
D_O2	1.175	cm^2/d		N	no		Broecker 1976
D_SO4	0.5184	cm^2/d		N	no		Furrer 1995
D_NH4_N14	1.02	cm^2/d		N1415	no		Furrer 1996
D_NH4_N15	1.02	cm^2/d		N1415	no		Furrer 1996
D_NO2_N14	1.02	cm^2/d		N1415	no		Furrer 1996
D_NO2_N15	1.02	cm^2/d		N1415	no		Furrer 1996
D_NO3_N14	1.02	cm^2/d		N1415	no		Furrer 1996
D_NO3_N15	1.02	cm^2/d		N1415	no		Furrer 1996
D_N2_N1414	0.959	cm^2/d		N1415	no		Broecker 1975
D_N2_N1415	0.959	cm^2/d		N1415	no		Broecker 1975
D_N2_N1515	0.959	cm^2/d		N1415	no		Broecker 1975
D_N2O_N1414	1.166	cm^2/d		N1415	no		Broecker 1975
D_N2O_N1415	1.166	cm^2/d		N1415	no		Broecker 1975
D_N2O_N1515	1.166	cm^2/d		N1415	no		Broecker 1975
a_tort	1.02	-	tortuosity factor	N	no		Maerki et al. 2004
m_tort	1.81	-	tortuosity exponent	N	no		Maerki et al. 2005
D_bio	1.0	cm^2/d		N	no		
depth_bio	1.0	cm		N	no		
f_Den2_Den1_pri	3.0	-		N	no	prior	
f_Den3_Den1_pri	3.0	-		N	no	prior	
f_Anam_Den2_pri	0.01	-		N	no	prior	determined from Cameron's data
f_DNRA1_Den1_pri	0.01	-		N	no	prior	
f_DNRA2_Den2_pri	0.01	-		N	no	prior	
f_Anam_side_pri	0.3	-		N	no	prior	Brunner 2013
a_N2O_Nit1_pri	0.2	uMol/L		N	no	prior	Ji 2018
b_N2O_Nit1_pri	0.08	-		N	no	prior	Ji 2018
K_O2_MinOx_pri	8.0	uMol/L		N	no	prior	Rooze 2016
K_O2_Den1_pri	3.0	uMol/L		N	no	prior	Wenk 2014
K_O2_Den2_pri	3.0	uMol/L		N	no	prior	Assumed to be the same as step 1 (Wenk 2014)
K_O2_Den3_pri	0.1	uMol/L		N	no	prior	I found out N2O reductase are highly inhibited by the presence of O2 (Suenaga 2018)
K_NO3_Den1_pri	2.46	uMol/L		N	no	prior	Su 2023
K_NO2_Den2_pri	0.41	uMol/L		N	no	prior	Su 2023
K_N2O_Den3_pri	3.7	uMol/L		N	no	prior	Suenaga 2018
K_O2_MinSulfRed_pri	5.0	uMol/L		N	no	prior	Parasha 2011
K_NO3_MinSulfRed_pri	5.0	uMol/L		N	no	prior	Parasha 2011
K_SO4_MinSulfRed_pri	20.0	uMol/L		N	no	prior	Richards 2016
K_O2_MinAnae_pri	5.0	uMol/L		N	no	prior	Parasha 2011
K_NO3_MinAnae_pri	5.0	uMol/L		N	no	prior	Parasha 2011
K_O2_Nit1_pri	3.5	uMol/L		N	no	prior	Martin 2019
K_O2_Nit2_pri	0.8	uMol/L		N	no	prior	Martin 2019
K_NH4_Nit1_pri	2.0	uMol/L		N	no	prior	Wyffels 2004
K_NO2_Nit2_pri	0.8	uMol/L		N	no	prior	Wyffels 2004
K_NH4_Anam_pri	1.0	uMol/L		N	no	prior	Wenk 2014
K_NO2_Anam_pri	5.0	uMol/L		N	no	prior	Reported for NO3 (Wenk 2014)
K_O2_Anam_pri	2.5	uMol/L		N	no	prior	Kalvelage 2011
K_O2_DNRA1_pri	3.0	uMol/L		N	no	prior	Assumed to be the same as Den1 (Wenk 2014)
K_O2_DNRA2_pri	3.0	uMol/L		N	no	prior	Assumed to be the same as Den2 (Wenk 2014)
K_NO3_DNRA1_pri	2.46	uMol/L		N	no	prior	Assumed to be the same as Den1 (Su 2023)
K_NO2_DNRA2_pri	0.41	uMol/L		N	no	prior	Assumed to be the same as Den1 (Su 2023)
gamma_NH4_MinOx_pri	0.14	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_Den1_pri	0.4	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_Den2_pri	0.4	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_Den3_pri	0.2	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_MinSulfRed_pri	0.3019	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_DNRA1_pri	0.25	-		N	no	prior	determined from Redfield stoichiometry
gamma_NH4_DNRA2_pri	0.75	-		N	no	prior	determined from Redfield stoichiometry
deltaN15_OM_pri	5.0	permille		N1415	no	prior	
eps_Den1_pri	20.0	permille		N1415	no	prior	A range of 15-25Â can be expected so we can center our estimates in the middle (Dale 2019)
eps_Den2_pri	15.0	permille		N1415	no	prior	A range of 8-25Â can be expected (Dale 2019) also because of difference between denitrifiers and nitrifier-denitrifiers (Denk 2017)
eps_Den3_pri	9.0	permille		N1415	no	prior	I found a more recent paper, where they give the range 4-13Â (Wenk 2016)
eps_Nit1_NO2_pri	30.0	permille		N1415	no	prior	Reported ranges are up to 35/38Â (Dale 2022 & Denk 2017)
eps_Nit1_N2O_pri	40.0	permille		N1415	no	prior	A range of 45-65Â can be expected (Denk 2017)
eps_Nit2_pri	-13.0	permille		N1415	no	prior	Denk 2017
eps_Anam_NH4_pri	23.0	permille		N1415	no	prior	Brunner 2013
eps_Anam_NO2_pri	16.0	permille		N1415	no	prior	Brunner 2014
eps_Anam_side_pri	-31.0	permille		N1415	no	prior	
eps_DNRA1_pri	20.0	permille		N1415	no	prior	expected to be the same as Den1
eps_DNRA2_pri	15.0	permille		N1415	no	prior	Not available in bibliography, but I think it makes sense to start with the same value as for Den2 since NO2 is being reduced in both 
sigmarel_K_pri	0.2	-		N	no	prior	
sigmarel_gamma_pri	0.1	-		N	no	prior	
sigmarel_f_Anam_side_pri	0.1			N	no	prior	
sigmarel_f_Den_pri	0.5			N	no	prior	
sigmarel_f_pri	0.25			N	no	prior	other f_ parameters than f_Anam_side
sigmarel_a_N2O_Nit1_pri	0.1			N	no	prior	
sigmarel_b_N2O_Nit1_pri	0.1			N	no	prior	
sigma_eps_pri	5.0	permille		N1415	no	prior	
sigma_deltaN15_OM_pri	0.5	permille			no	prior	
sigma_C_a_pri	0.5	uMol/L			no	prior likelihood	
sigma_C_b_pri	1.0	uMol/L			no	prior likelihood	
sigma_delta_pri	0.5	permille			no	prior likelihood	
sigmarel_sigma_pri	0.5				no	prior likelihood	
depth	5.0	cm	depth of modeled sediment layer	config	no		
z1	0.0	cm	porosity interpolation point 1	config	no		
por1	0.95	-	porosity value for z <= z1	config	no		
z2	1.0	cm	porosity interpolation point 2	config	no		
por2	0.9	-	porosity value for z = z2	config	no		
z3	2.0	cm	porosity interpolation point 3	config	no		
por3	0.8	-	porosity value for z >= z3	config	no		
t_start	0.0	d	simulation starting time	config	no		
t_end	100.0	d	simulation end time	config	no		
n_t	11.0	-	number of time output points (equally spaced between t_start and t_end)	config	no		
n_cells	50.0	-	number of cells for sediment layer discretization	num	no		
f_cellsize	5.0	-	factor of cellsize between deepest cell and near surface cell (1 leads to equal cell sizes)	num	no		
reltol_int	1.0e-5	-	relative tolerance of the time integration algorithm	num	no		
abstol_int	1.0e-5	uMol/L	absolute tolerance of the time integration algorithm	num	no		
maxiters_int	10000.0	-	maximum number of iterations for numerical integration	num	no		
dt_ini_int	0.001	d	initial time step for numerical integration	num	no		
maxiters_opt	100.0	-	maximum number of iterations for maximum posterior estimation	num	no		
n_samples_MCMC	5000.0	-	Markov chain length for Bayesian inference	num	no		
n_samples_HMC	100.0	-	Markov chain length for Bayesian inference with Hamiltonian Monte Carlo algorithms	num	no		
thin_MCMC	5.0	-	thinning factor for Markov chains (1 means no thinning, thinning is not applied for HMC)	num	no		
n_leapfrog_HMC	10.0	-	number of leapfrog steps for AdvancedHMC (0 means NUTS, >0 fixed number of leapfrog steps)	num	no		
step_initial_HMC	0.01	-	initial step size for AdvancedHMC (<=0 means automatic choice; do this first and check output for specifying a reasonable step size)	num	no		
thin_predict	5.0	-	thinning factor for uncertainty propagation from MCMC sample (1 means no thinning)	num	no		
